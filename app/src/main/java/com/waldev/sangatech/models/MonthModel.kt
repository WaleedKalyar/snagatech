package com.waldev.sangatech.models

data class MonthModel(
    val month: String = "",
    var earnings: Double
)
