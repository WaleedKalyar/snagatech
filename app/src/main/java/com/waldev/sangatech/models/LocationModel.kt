package com.waldev.sangatech.models

import java.io.Serializable

data class LocationModel(
    var latitude: Double = 0.0,
    var longitude: Double = 0.0
) : Serializable
