package com.waldev.sangatech.requests

import okhttp3.MultipartBody

data class UpdateUserRequest(
    var id: Int,
    var name: String? = null,
    var email: String,
    var password: String? = null,
    var category: Int? = null,
    var address_1: String? = null,
    var address_2: String? = null,
    var longitude: Double? = null,
    var latitude: Double? = null,
    var profile_pic: MultipartBody.Part? = null,
    var desc: String? = null,
)
