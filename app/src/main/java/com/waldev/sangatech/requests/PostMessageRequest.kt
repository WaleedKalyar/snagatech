package com.waldev.sangatech.requests

import okhttp3.MultipartBody

data class PostMessageRequest(
    private var conversation_id: Int? = null,
    private var sender_id: Int? = null,
    private var receiver_id: Int? = null,
    private var msg: String? = null,
    private var file: String? = null,
    private var file_url: MultipartBody.Part? = null,
    private var type: String? = null,
)
