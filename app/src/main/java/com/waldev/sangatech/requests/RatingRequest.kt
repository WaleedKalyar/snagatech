package com.waldev.sangatech.requests

data class RatingRequest(
    var work_id: Int? = null,
    var cus_id: Int? = null,
    var worker_id: Int? = null,
    var rate: Float? = null,
    var review: String? = null
)
