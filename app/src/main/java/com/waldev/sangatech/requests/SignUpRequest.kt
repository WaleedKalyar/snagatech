package com.waldev.sangatech.requests

data class SignUpRequest(
    var name: String,
    var email: String,
    var password: String,
    var category: Int,
    var address1: String,
    var address2: String,
    var latitude: Double,
    var longitude: Double
) {
}