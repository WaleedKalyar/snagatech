package com.waldev.sangatech.requests

data class PostWorkRequest(
    var id: Int? = null,
    var customer_id: Int,
    var worker_id: Int,
    var cat_id: Int,
    var title: String,
    var amount: Double,
    var address: String,
    var desc: String,
    var arrival_time: String
)