package com.waldev.sangatech.responses.chat

import com.waldev.sangatech.enums.ResponseStatus

data class PostMessageResponse(
    var data: Data = Data(),
    var status: ResponseStatus = ResponseStatus.idle
) {
    data class Data(
        var conversation: Conversation = Conversation()
    ) {
        data class Conversation(
            var conversation_id: String = "",
            var created_at: String = "",
            var id: Int = 0,
            var msg: String = "",
            var receiver_id: String = "",
            var sender_id: String = "",
            var type: String = "",
            var updated_at: String = ""
        )
    }
}