package com.waldev.sangatech.responses.user

data class ForgotPasswordResponse(
    var message: String = "",
    var status: String = ""
)