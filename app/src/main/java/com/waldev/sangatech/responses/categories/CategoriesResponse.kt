package com.waldev.sangatech.responses.categories

import com.waldev.sangatech.enums.ResponseStatus

data class CategoriesResponse(
    var data: Data = Data(),
    var message: String = "",
    var status: ResponseStatus = ResponseStatus.idle
) {
    data class Data(
        var catDetail: List<CatDetail> = listOf()
    ) {
        data class CatDetail(
            var created_at: String = "",
            var id: Int = 0,
            var name: String = "",
            var status: Int = 0,
            var updated_at: String = "",
            var thumbnail: String? = null,
            var thumbnail_url: String? = null
        ) {
            override fun toString(): String {
                return name
            }
        }
    }
}