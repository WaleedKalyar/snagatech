package com.waldev.sangatech.responses.user

import com.waldev.sangatech.enums.ResponseStatus

data class UserDetailsResponse(
    var data: Data = Data(),
    var status: ResponseStatus = ResponseStatus.idle,
    var message: String = ""
) {
    data class Data(
        var catUsers: CatUsers = CatUsers()
    ) {
        data class CatUsers(
            var address_1: String = "",
            var address_2: String = "",
            var category: String = "",
            var created_at: String = "",
            var desc: String = "",
            var earnings: List<Double> = listOf(),
            var email: String = "",
            var latitude: String = "",
            var longitude: String = "",
            var name: String = "",
            var profile_img: String? = null,
            var profile_url: String? = null,
            var rating: Float = 0.0f,
            var role: String = "",
            var status: Int = 0,
            var total_earning: Double = 0.0,
            var type: String = "",
            var updated_at: Any = Any()
        )
    }
}