package com.waldev.sangatech.responses.rating

import com.waldev.sangatech.enums.ResponseStatus

data class WorkerRatingsResponse(
    var `data`: Data = Data(),
    var status: ResponseStatus = ResponseStatus.idle,
    var message: String = ""
) {
    data class Data(
        var ratings: List<Rating> = listOf()
    ) {
        data class Rating(
            var created_at: String = "",
            var cus_id: Int = 0,
            var customer_detail: CustomerDetail = CustomerDetail(),
            var id: Int = 0,
            var rate: Double = 0.0,
            var review: String = "",
            var updated_at: String = "",
            var work_id: Int = 0,
            var worker_detail: WorkerDetail = WorkerDetail(),
            var worker_id: Int = 0
        ) {
            data class CustomerDetail(
                var address_1: Any = Any(),
                var address_2: Any = Any(),
                var category: Any = Any(),
                var created_at: String = "",
                var desc: Any = Any(),
                var email: String = "",
                var email_verified_at: Any = Any(),
                var id: Int = 0,
                var latitude: String = "",
                var longitude: String = "",
                var name: String = "",
                var profile_img: String? = null,
                var profile_url: String? = null,
                var role: String = "",
                var status: Int = 0,
                var type: String = "",
                var updated_at: String = ""
            )

            data class WorkerDetail(
                var address_1: Any = Any(),
                var address_2: Any = Any(),
                var category: Int = 0,
                var created_at: String = "",
                var desc: String = "",
                var email: String = "",
                var email_verified_at: Any = Any(),
                var id: Int = 0,
                var latitude: String = "",
                var longitude: String = "",
                var name: String = "",
                var profile_img: String? = null,
                var profile_url: String? = null,
                var role: String = "",
                var status: Int = 0,
                var type: String = "",
                var updated_at: String = ""
            )
        }
    }
}