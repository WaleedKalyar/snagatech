package com.waldev.sangatech.responses.chat

import com.waldev.sangatech.enums.MessageType
import com.waldev.sangatech.enums.ResponseStatus

data class ConversationMessagesResponse(
    var data: Data = Data(),
    var message: String = "",
    var status: ResponseStatus = ResponseStatus.idle
) {
    data class Data(
        var conversation: List<Conversation> = listOf()
    ) {
        data class Conversation(
            var conversation_id: Int = 0,
            var created_at: String = "",
            var `file`: String? = null,
            var file_url: String? = null,
            var get_conversation: GetConversation = GetConversation(),
            var get_receiver_detail: GetReceiverDetail = GetReceiverDetail(),
            var get_sender_detail: GetSenderDetail = GetSenderDetail(),
            var id: Int = 0,
            var msg: String = "",
            var receiver_id: Int = 0,
            var sender_id: Int = 0,
            var type: MessageType = MessageType.NONE,
            var updated_at: String = ""
        ) {
            data class GetConversation(
                var created_at: String = "",
                var cus_id: Int = 0,
                var id: Int = 0,
                var status: Int = 0,
                var updated_at: String = "",
                var work_req_id: Int = 0,
                var worker_id: Int = 0
            )

            data class GetReceiverDetail(
                var address_1: String = "",
                var address_2: String = "",
                var category: Int = 0,
                var created_at: String = "",
                var desc: String = "",
                var email: String = "",
                var email_verified_at: Any = Any(),
                var id: Int = 0,
                var latitude: String = "",
                var longitude: String = "",
                var name: String = "",
                var profile_img: String = "",
                var profile_url: String = "",
                var role: String = "",
                var status: Int = 0,
                var type: String = "",
                var updated_at: String = ""
            )

            data class GetSenderDetail(
                var address_1: String = "",
                var address_2: String = "",
                var category: Any = Any(),
                var created_at: String = "",
                var desc: Any = Any(),
                var email: String = "",
                var email_verified_at: Any = Any(),
                var id: Int = 0,
                var latitude: String = "",
                var longitude: String = "",
                var name: String = "",
                var profile_img: Any = Any(),
                var profile_url: Any = Any(),
                var role: String = "",
                var status: Int = 0,
                var type: String = "",
                var updated_at: String = ""
            )
        }
    }
}