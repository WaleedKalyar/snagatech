package com.waldev.sangatech.responses.work

import com.waldev.sangatech.enums.ResponseStatus

data class WorkResponse(
    var data: Data = Data(),
    var message: ResponseStatus = ResponseStatus.idle,
    var status: String = ""
) {
    data class Data(
        var workReq: List<WorkReq> = listOf()
    ) {
        data class WorkReq(
            var address: String = "",
            var amount: Double = 0.0,
            var arr_time: String = "",
            var cat_id: Int = 0,
            var created_at: String = "",
            var cus_id: Int = 0,
            var desc: String = "",
            var id: Int = 0,
            var status: Int = 0,
            var title: String = "",
            var updated_at: String = "",
            var worker_id: Int = 0,
            var cus_name: String = "",
            var customer_detail: CustomerDetail = CustomerDetail(),
            var worker_name: String = "",
            var worker_detail: WorkerDetail = WorkerDetail(),
        ){
            data class CustomerDetail(
                var address_1: Any = Any(),
                var address_2: Any = Any(),
                var category: Any = Any(),
                var created_at: String = "",
                var desc: Any = Any(),
                var email: String = "",
                var email_verified_at: Any = Any(),
                var id: Int = 0,
                var latitude: String = "",
                var longitude: String = "",
                var name: String = "",
                var profile_img: Any = Any(),
                var profile_url: Any = Any(),
                var role: String = "",
                var status: Int = 0,
                var type: String = "",
                var updated_at: String = ""
            )

            data class WorkerDetail(
                var address_1: Any = Any(),
                var address_2: Any = Any(),
                var category: Int = 0,
                var created_at: String = "",
                var desc: String = "",
                var email: String = "",
                var email_verified_at: Any = Any(),
                var id: Int = 0,
                var latitude: String = "",
                var longitude: String = "",
                var name: String = "",
                var profile_img: String = "",
                var profile_url: String = "",
                var role: String = "",
                var status: Int = 0,
                var type: String = "",
                var updated_at: String = ""
            )
        }
    }
}