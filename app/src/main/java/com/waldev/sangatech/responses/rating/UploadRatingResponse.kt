package com.waldev.sangatech.responses.rating

import com.waldev.sangatech.enums.ResponseStatus

data class UploadRatingResponse(
    var data: Data = Data(),
    var message: String = "",
    var status: ResponseStatus = ResponseStatus.idle
) {
    data class Data(
        var workReq: WorkReq = WorkReq()
    ) {
        data class WorkReq(
            var created_at: String = "",
            var cus_id: String = "",
            var id: Int = 0,
            var rate: String = "",
            var review: String = "",
            var updated_at: String = "",
            var work_id: String = "",
            var worker_id: String = ""
        )
    }
}