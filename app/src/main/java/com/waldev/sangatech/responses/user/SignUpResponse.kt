package com.waldev.sangatech.responses.user

import com.waldev.sangatech.enums.ResponseStatus

data class SignUpResponse(
    var data: Data = Data(),
    var status: ResponseStatus = ResponseStatus.idle,
    var message: String = ""
) {
    data class Data(
        var userDetail: UserDetail = UserDetail()
    ) {
        data class UserDetail(
            var address_1: String = "",
            var address_2: String = "",
            var category: String = "",
            var created_at: String = "",
            var email: String = "",
            var id: Int = 0,
            var latitude: String = "",
            var longitude: String = "",
            var name: String = "",
            var role: String = "",
            var type: String = "",
            var updated_at: String = ""
        )
    }
}