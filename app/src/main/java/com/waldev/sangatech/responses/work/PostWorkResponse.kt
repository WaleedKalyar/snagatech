package com.waldev.sangatech.responses.work

import com.waldev.sangatech.enums.ResponseStatus

data class PostWorkResponse(
    var data: Data = Data(),
    var status: ResponseStatus = ResponseStatus.idle,
    var message: String = ""
) {
    data class Data(
        var workReq: WorkReq = WorkReq()
    ) {
        data class WorkReq(
            var address: String = "",
            var amount: String = "",
            var arr_time: String = "",
            var cat_id: Int = 0,
            var created_at: String = "",
            var cus_id: Int = 0,
            var desc: String = "",
            var id: Int = 0,
            var title: String = "",
            var updated_at: String = "",
            var worker_id: Int = 0
        )
    }
}