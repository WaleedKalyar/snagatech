package com.waldev.sangatech.responses.user

data class UserUpdateResponse(
    var data: Data = Data(),
    var message: String = "",
    var status: String = ""
) {
    data class Data(
        var userDetail: UserDetail = UserDetail()
    ) {
        data class UserDetail(
            var address_1: String = "",
            var address_2: String = "",
            var category: String = "",
            var created_at: String = "",
            var desc: String = "",
            var email: String = "",
            var email_verified_at: String = "",
            var id: Int = 0,
            var latitude: String = "",
            var longitude: String = "",
            var name: String = "",
            var profile_img: String = "",
            var profile_url: String = "",
            var role: String = "",
            var status: Int = 0,
            var type: String = "",
            var updated_at: String = ""
        )
    }
}