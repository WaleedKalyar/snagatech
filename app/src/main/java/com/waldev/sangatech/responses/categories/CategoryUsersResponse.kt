package com.waldev.sangatech.responses.categories

import com.waldev.sangatech.enums.ResponseStatus

data class CategoryUsersResponse(
    var data: Data = Data(),
    var message: String = "",
    var status: ResponseStatus = ResponseStatus.idle
) {
    data class Data(
        var catUsers: List<CatUser> = listOf()
    ) {
        data class CatUser(
            var address_1: String = "",
            var address_2: String = "",
            var category: Int = 0,
            var created_at: String = "",
            var desc: String = "",
            var email: String = "",
            var email_verified_at: String? = "",
            var id: Int = 0,
            var latitude: String = "",
            var longitude: String = "",
            var name: String = "",
            var profile_img: String? = "",
            var profile_url: String? = "",
            var role: String = "",
            var status: Int = 0,
            var type: String = "",
            var updated_at: String = ""
        )
    }
}