package com.waldev.sangatech.utils;

import android.content.Context;

import androidx.core.content.ContextCompat;


import com.waldev.sangatech.R;

import io.github.muddz.styleabletoast.StyleableToast;


public class ToastUtility {

    public static void errorToast(Context context, String message) {

        new StyleableToast.Builder(context)
                .backgroundColor(ContextCompat.getColor(context, R.color.colorRed))
                .text(message).textColor(ContextCompat.getColor(context, R.color.colorWhite))
                .solidBackground().textSize(12).cornerRadius(8).font(R.font.sf_ui_display_regular)
                .show();

    }

    public static void successToast(Context context, String message) {

        new StyleableToast.Builder(context)
                .backgroundColor(ContextCompat.getColor(context, R.color.colorBlueDark))
                .text(message).textColor(ContextCompat.getColor(context, R.color.colorWhite))
                .solidBackground().textSize(12).cornerRadius(8).font(R.font.sf_ui_display_regular)
                .show();
    }

    public static void warningToast(Context context, String message) {

        new StyleableToast.Builder(context)
                .backgroundColor(ContextCompat.getColor(context, R.color.colorYellow))
                .text(message).textColor(ContextCompat.getColor(context, R.color.colorWhite))
                .solidBackground().cornerRadius(8).textSize(12).font(R.font.sf_ui_display_regular)
                .show();

    }

}
