package com.waldev.sangatech.utils

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.google.gson.Gson
import com.waldev.sangatech.R
import com.waldev.sangatech.responses.user.LoginResponse
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDataStore @Inject constructor(@ApplicationContext val context: Context) {

    private val Context.appDataStore by preferencesDataStore(name = context.resources.getString(R.string.app_name))

    // keys
    companion object {
        private val USER_MODEL = stringPreferencesKey("user-model")
    }


    suspend fun saveUserModel(user: LoginResponse.Data.UserDetail) {
        context.appDataStore.edit { mutablePreferences ->
            var userJson = Gson().toJson(user)
            userJson = userJson.replace("\\\\", "")
            mutablePreferences[USER_MODEL] = userJson
        }
    }

    fun getUserFromDataStore(): Flow<LoginResponse.Data.UserDetail> =
        context.appDataStore.data.map { preferences ->
            val jsonUser = preferences[USER_MODEL]
            if (jsonUser.isNullOrEmpty()) {
                LoginResponse.Data.UserDetail()
            } else {
                Gson().fromJson(jsonUser, LoginResponse.Data.UserDetail::class.java)
            }

        }


}