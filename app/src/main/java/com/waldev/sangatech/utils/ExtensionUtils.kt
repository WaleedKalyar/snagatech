package com.waldev.sangatech.utils

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

object ExtensionUtils {
    fun <T> Fragment.collectLatestLifecycleFlow(flow: Flow<T>, collect: suspend (T) -> Unit) {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                flow.collectLatest(collect)
            }
        }
    }

    fun Fragment.hideWaitingDialogIfVisible(waitingDialog: WaitingDialog) {
        parentFragment.let { }
        if (waitingDialog.isShowing) {
            waitingDialog.dismiss()
        }
    }

    fun Fragment.showOrUpdateWaitingDialog(waitingDialog: WaitingDialog, message: String?) {
        if (waitingDialog.isShowing) {
            waitingDialog.setStatusTextView(message)
        } else {
            waitingDialog.show(message)
        }

    }

}