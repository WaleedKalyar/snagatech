package com.waldev.sangatech.utils

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object DateUtilities {

    fun getDateFromMillis(millis: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        val format = SimpleDateFormat("MMMM, dd, yyyy", Locale.getDefault())
        return format.format(calendar.time)
    }

    fun getTimeFromMillis(millis: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        val format = SimpleDateFormat("hh:mm aa", Locale.getDefault())
        return format.format(calendar.time)
    }


    fun getMillisFromStringDate(strDate: String): Long {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
        val cal = Calendar.getInstance()
        val date = format.parse(strDate)
        cal.time = date
        return cal.timeInMillis
    }


    fun getTimeAgo(timestamp: Long): String {

        val suffix = "Ago"

        val difference = Date().time - timestamp

        val day = TimeUnit.MILLISECONDS.toDays(difference)

        val hour = TimeUnit.MILLISECONDS.toHours(difference)

        val minute = TimeUnit.MILLISECONDS.toMinutes(difference)

        val second = TimeUnit.MILLISECONDS.toSeconds(difference)

        return if (second < 10) {
            "Just Now"
        } else if (second < 60) {
            if (second == 1L) "1 Second $suffix" else "$second Seconds $suffix"
        } else if (minute < 60) {
            if (minute == 1L) "1 Minute $suffix" else "$minute Minutes $suffix"
        } else if (hour < 24) {
            if (hour == 1L) "1 Hour $suffix" else "$hour Hours $suffix"
        } else if (day >= 360) {
            if (day == 360L) "1 Years $suffix" else (day / 360).toString() + " Years " + suffix
        } else if (day >= 30) {
            if (day < 60) "1 Month $suffix" else (day / 30).toString() + " Months " + suffix
        } else if (day >= 7) {
            if (day < 14) "1 Week $suffix" else (day / 7).toString() + " Weeks " + suffix
        } else {
            if (day == 1L) "1 Day $suffix" else "$day Days $suffix"
        }
    }
}