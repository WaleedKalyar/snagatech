package com.waldev.sangatech.utils

import com.waldev.sangatech.models.MonthModel

object AppConsts {
    const val APP_BASE_URL = "http://18.117.156.62"

    const val USER_TYPE_WORKER = "worker"
    const val USER_TYPE_CUSTOMER = "customer"

    const val CATEGORY_ID = "cat-id"
    const val CATEGORY_NAME = "cat-name"

    const val WORKER_ID = "worker-id"
    const val WORKER_NAME = "worker-name"

    const val USER_ID = "user-id"


    fun getGeneric12Months() = mutableListOf(
        MonthModel("JAN", 0.0),
        MonthModel("FEB", 0.0),
        MonthModel("MAR", 0.0),
        MonthModel("APR", 0.0),
        MonthModel("MAY", 0.0),
        MonthModel("JUN", 0.0),
        MonthModel("JUL", 0.0),
        MonthModel("AUG", 0.0),
        MonthModel("SEP", 0.0),
        MonthModel("OCT", 0.0),
        MonthModel("NOV", 0.0),
        MonthModel("DEC", 0.0),
    )
}