package com.waldev.sangatech.utils

import android.content.Context
import android.location.LocationManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.CancellationTokenSource
import com.waldev.sangatech.models.LocationModel
import timber.log.Timber


object SingleTimeLocationProvider {
    fun requestSingleUpdate(context: Context, callback: LocationCallback) {
        val providerClient: FusedLocationProviderClient = LocationServices
            .getFusedLocationProviderClient(context)
        val tokenSource = CancellationTokenSource()
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val isGPSEnabled = locationManager
            .isProviderEnabled(LocationManager.GPS_PROVIDER)
        val isNetworkEnable = locationManager
            .isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        if (isGPSEnabled && isNetworkEnable) {
            try {
                providerClient.lastLocation.addOnSuccessListener { lastLocation ->
                    if (lastLocation != null) {
                        Timber.d(
                            "Lat -> %s Lng -> %s",
                            lastLocation.latitude,
                            lastLocation.longitude
                        )
                        val location =
                            LocationModel(lastLocation.latitude, lastLocation.longitude)
                        callback.onLocationRequestComplete()
                        callback.onLocationAvailable(location)
                    } else {
                        providerClient.getCurrentLocation(
                            PRIORITY_HIGH_ACCURACY,
                            tokenSource.token
                        ).addOnSuccessListener { freshLocation ->
                            Timber.d(
                                "Lat -> %s Lng -> %s",
                                freshLocation.latitude,
                                freshLocation.longitude
                            )
                            val location =
                                LocationModel(freshLocation.latitude, freshLocation.longitude)
                            callback.onLocationRequestComplete()
                            callback.onLocationAvailable(location)
                        }.addOnFailureListener { exception ->
                            Timber.d("Location Fetch Error -> %s", exception.localizedMessage)
                            callback.onLocationRequestComplete()
                            callback.onLocationNotAvailable()
                        }
                    }
                }
            } catch (exception: SecurityException) {
                Timber.d("Security Exception -> %s", exception.message)
                callback.onLocationRequestComplete()
                callback.onLocationPermissionError()
            }
        } else {
            callback.onLocationRequestComplete()
            callback.onLocationProvidersNotEnable()
        }
    }

    interface LocationCallback {
        fun onLocationNotAvailable()
        fun onLocationRequestComplete()
        fun onLocationPermissionError()
        fun onLocationProvidersNotEnable()
        fun onLocationAvailable(location: LocationModel?)
    }


}