package com.waldev.sangatech.repos.rating

import com.waldev.sangatech.network.BaseApiResponse
import com.waldev.sangatech.network.RemoteDataSource
import com.waldev.sangatech.requests.RatingRequest
import com.waldev.sangatech.responses.rating.UploadRatingResponse
import com.waldev.sangatech.responses.rating.WorkerRatingsResponse
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class RatingRepoImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : RatingRepo, BaseApiResponse() {

    override fun postRating(request: RatingRequest): Flow<NetworkResult<UploadRatingResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.sendRatingFeedback(request) })
        }.flowOn(Dispatchers.IO)

    override fun getRatingsByWorkerId(workerId: Int): Flow<NetworkResult<WorkerRatingsResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getWorkerRatings(workerId) })
        }.flowOn(Dispatchers.IO)

}