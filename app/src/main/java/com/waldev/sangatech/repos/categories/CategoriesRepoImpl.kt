package com.waldev.sangatech.repos.categories

import com.waldev.sangatech.network.BaseApiResponse
import com.waldev.sangatech.network.RemoteDataSource
import com.waldev.sangatech.responses.categories.CategoriesResponse
import com.waldev.sangatech.responses.categories.CategoryUsersResponse
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class CategoriesRepoImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : CategoriesRepo, BaseApiResponse() {

    override fun getAllCategories(): Flow<NetworkResult<CategoriesResponse>> = flow {
        emit(NetworkResult.Loading())
        emit(safeApiCall { remoteDataSource.getAllCategories() })
    }.flowOn(Dispatchers.IO)

    override fun getAllUsersByCategory(catId: Int): Flow<NetworkResult<CategoryUsersResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getAllUsersOfCategory(catId) })
        }.flowOn(Dispatchers.IO)

}