package com.waldev.sangatech.repos.chat

import com.waldev.sangatech.requests.PostMessageRequest
import com.waldev.sangatech.responses.chat.PostMessageResponse
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.flow.Flow

interface ChatRepo {
    fun sendNewMessage(request: PostMessageRequest): Flow<NetworkResult<PostMessageResponse>>
    fun observeAllMessagesInConversation(subscriptionId:String):Flow<String>
}