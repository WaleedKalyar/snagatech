package com.waldev.sangatech.repos.rating

import com.waldev.sangatech.requests.RatingRequest
import com.waldev.sangatech.responses.rating.UploadRatingResponse
import com.waldev.sangatech.responses.rating.WorkerRatingsResponse
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.flow.Flow

interface RatingRepo {

    fun postRating(request: RatingRequest): Flow<NetworkResult<UploadRatingResponse>>

    fun getRatingsByWorkerId(workerId:Int): Flow<NetworkResult<WorkerRatingsResponse>>
}