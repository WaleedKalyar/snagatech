package com.waldev.sangatech.repos.chat

import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.waldev.sangatech.network.BaseApiResponse
import com.waldev.sangatech.network.RemoteDataSource
import com.waldev.sangatech.requests.PostMessageRequest
import com.waldev.sangatech.responses.chat.PostMessageResponse
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class ChatRepoImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : ChatRepo, BaseApiResponse() {
    override fun sendNewMessage(request: PostMessageRequest): Flow<NetworkResult<PostMessageResponse>> =
        flow {
            emit(safeApiCall { remoteDataSource.sendNewMessage(request) })
        }.flowOn(Dispatchers.IO)

    override fun observeAllMessagesInConversation(subscriptionId: String): Flow<String> =
        callbackFlow<String> {
            val options = PusherOptions()
            options.setCluster("PUSHER_APP_CLUSTER")

            val pusher = Pusher("PUSHER_APP_KEY", options)
            val channel = pusher.subscribe(subscriptionId)

            channel.bind(
                "EVENT_NAME"
            ) {
                trySend(it.data)
            }
            pusher.connection
            awaitClose { close() }
        }.flowOn(Dispatchers.IO)


}