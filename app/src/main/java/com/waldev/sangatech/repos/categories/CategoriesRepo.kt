package com.waldev.sangatech.repos.categories

import com.waldev.sangatech.responses.categories.CategoriesResponse
import com.waldev.sangatech.responses.categories.CategoryUsersResponse
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.flow.Flow

interface CategoriesRepo {
    fun getAllCategories(): Flow<NetworkResult<CategoriesResponse>>
    fun getAllUsersByCategory(catId: Int): Flow<NetworkResult<CategoryUsersResponse>>
}