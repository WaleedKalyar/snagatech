package com.waldev.sangatech.repos.work

import com.waldev.sangatech.requests.PostWorkRequest
import com.waldev.sangatech.responses.work.MarkCompleteWorkResponse
import com.waldev.sangatech.responses.work.PostWorkResponse
import com.waldev.sangatech.responses.work.UpdateWorkStateResponse
import com.waldev.sangatech.responses.work.WorkResponse
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.flow.Flow

interface WorkRepo {

    fun addNewWorkRequest(workRequest: PostWorkRequest): Flow<NetworkResult<PostWorkResponse>>

    fun getCustomerPendingRequests(customerId: Int): Flow<NetworkResult<WorkResponse>>
    fun getCustomerRunningRequests(customerId: Int): Flow<NetworkResult<WorkResponse>>
    fun getCustomerRejectedRequests(customerId: Int): Flow<NetworkResult<WorkResponse>>
    fun getCustomerCompletedRequests(customerId: Int): Flow<NetworkResult<WorkResponse>>
    fun customerMarkRequestCompleted(
        customerId: Int,
        workId: Int
    ): Flow<NetworkResult<MarkCompleteWorkResponse>>

    fun getWorkerRecentOrders(workerId: Int): Flow<NetworkResult<WorkResponse>>
    fun getWorkerPendingRequests(workerId: Int): Flow<NetworkResult<WorkResponse>>
    fun getWorkerRunningRequests(workerId: Int): Flow<NetworkResult<WorkResponse>>
    fun getWorkerReceivedRequests(workerId: Int): Flow<NetworkResult<WorkResponse>>
    fun getWorkerCompletedRequests(workerId: Int): Flow<NetworkResult<WorkResponse>>
    fun workAcceptByWorkerRequest(
        workerId: Int,
        workId: Int
    ): Flow<NetworkResult<UpdateWorkStateResponse>>

    fun workRejectByWorkerRequest(
        workerId: Int,
        workId: Int
    ): Flow<NetworkResult<UpdateWorkStateResponse>>

}