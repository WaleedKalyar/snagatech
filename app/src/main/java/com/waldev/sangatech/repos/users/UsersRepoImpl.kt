package com.waldev.sangatech.repos.users

import com.waldev.sangatech.network.BaseApiResponse
import com.waldev.sangatech.network.RemoteDataSource
import com.waldev.sangatech.requests.SignUpRequest
import com.waldev.sangatech.requests.UpdateUserRequest
import com.waldev.sangatech.responses.user.ForgotPasswordResponse
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.responses.user.SignUpResponse
import com.waldev.sangatech.responses.user.UserDetailsResponse
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import timber.log.Timber
import java.io.File
import javax.inject.Inject


class UsersRepoImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : UsersRepo, BaseApiResponse() {

    override fun loginUserWithEmailAndPassword(
        email: String,
        password: String
    ): Flow<NetworkResult<LoginResponse>> = flow {
        emit(NetworkResult.Loading())
        emit(safeApiCall {
            remoteDataSource.loginUserWithEmailAndPassword(email, password)
        })
    }.flowOn(Dispatchers.IO)

    override fun registerUserWithEmailAndPassword(
        signUpRequest: SignUpRequest
    ): Flow<NetworkResult<SignUpResponse>> = flow {
        emit(NetworkResult.Loading())
        emit(
            safeApiCall {
                remoteDataSource.registerUserWithEmailAndPassword(signUpRequest)
            }
        )
    }.flowOn(Dispatchers.IO)

//    override fun updateUserInfo(updateRequest: UpdateUserRequest): Flow<NetworkResult<UserUpdateResponse>> =
//        flow {
//            emit(NetworkResult.Loading())
//            emit(safeApiCall {
//                remoteDataSource.updateUserInfo(updateRequest)
//            })
//        }.flowOn(Dispatchers.IO)

    override fun forgotPassword(email: String): Flow<NetworkResult<ForgotPasswordResponse>> = flow {
        emit(NetworkResult.Loading())
        emit(safeApiCall {
            remoteDataSource.forgotPassword(email)
        })
    }.flowOn(Dispatchers.IO)

    override fun updateUserProfile(
        id: Int,
        name: String?,
        email: String,
        password: String?,
        category: Int?,
        address1: String?,
        address2: String?,
        latitude: Double?,
        longitude: Double?,
        profile_pic: File?,
        desc: String?
    ): Flow<NetworkResult<LoginResponse>> = flow {
        val reqFile: RequestBody? = profile_pic?.asRequestBody("image/*".toMediaTypeOrNull())
        var multipartBody: MultipartBody.Part? = null
        if (reqFile != null) {
            multipartBody =
                MultipartBody.Part.createFormData("profile_pic", profile_pic.name, reqFile)
        }

        Timber.d("Image Request body -> ${reqFile?.toString()}")
        emit(NetworkResult.Loading())
        val request =  UpdateUserRequest(
            id = id,
            email = email,
            name = name,
            password = password,
            category = category,
            address_1 = address1,
            address_2 = address2,
            latitude = latitude,
            longitude = longitude,
            profile_pic = multipartBody,
            desc = desc
        )
       val builder = MultipartBody.Builder()
           .setType(MultipartBody.FORM)

        builder.addFormDataPart("id",id.toString())
        builder.addFormDataPart("email",email)
        if (!name.isNullOrEmpty()){
            builder.addFormDataPart("name",name)
        }
        if (category != null && category>0){
            builder.addFormDataPart("category",category.toString())
        }
        if (!address1.isNullOrEmpty()){
            builder.addFormDataPart("address_1",address1)
        }
        if (!address2.isNullOrEmpty()){
            builder.addFormDataPart("address_2",address2)
        }

        if (latitude != null && latitude>0){
            builder.addFormDataPart("latitude",latitude.toString())
        }
        if (longitude != null && longitude>0){
            builder.addFormDataPart("longitude",longitude.toString())
        }
        if (!desc.isNullOrEmpty()){
            builder.addFormDataPart("desc",desc)
        }
        if (multipartBody!=null){
            builder.addPart(multipartBody)
        }



        emit(safeApiCall {
            remoteDataSource.updateUserInfo(
                builder.build()
            )
//            remoteDataSource.updateUserProfile(
//                id,
//                name,
//                email,
//                password,
//                category,
//                address1,
//                address2,
//                latitude,
//                longitude,
//                multipartBody,
//                desc
//            )
        })
    }.flowOn(Dispatchers.IO)

    override fun getWorkerDetailsId(workerId: Int): Flow<NetworkResult<UserDetailsResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getWorkerDetailsById(workerId) })
        }.flowOn(Dispatchers.IO)


}