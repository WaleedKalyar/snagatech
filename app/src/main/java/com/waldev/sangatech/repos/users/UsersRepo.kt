package com.waldev.sangatech.repos.users

import com.waldev.sangatech.requests.SignUpRequest
import com.waldev.sangatech.requests.UpdateUserRequest
import com.waldev.sangatech.responses.user.*
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.flow.Flow
import java.io.File


interface UsersRepo {
    fun loginUserWithEmailAndPassword(
        email: String,
        password: String
    ): Flow<NetworkResult<LoginResponse>>

    fun registerUserWithEmailAndPassword(
        signUpRequest: SignUpRequest
    ): Flow<NetworkResult<SignUpResponse>>

    //fun updateUserInfo(updateRequest: UpdateUserRequest): Flow<NetworkResult<UserUpdateResponse>>

    fun forgotPassword(email: String): Flow<NetworkResult<ForgotPasswordResponse>>

    fun updateUserProfile(
        id: Int,
        name: String?,
        email: String,
        password: String?,
        category: Int?,
        address1: String?,
        address2: String?,
        latitude: Double?,
        longitude: Double?,
        profile_pic: File?,
        desc: String?
    ): Flow<NetworkResult<LoginResponse>>

    fun getWorkerDetailsId(workerId:Int): Flow<NetworkResult<UserDetailsResponse>>
}