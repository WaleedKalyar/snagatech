package com.waldev.sangatech.repos.work

import com.waldev.sangatech.network.BaseApiResponse
import com.waldev.sangatech.network.RemoteDataSource
import com.waldev.sangatech.requests.PostWorkRequest
import com.waldev.sangatech.responses.work.MarkCompleteWorkResponse
import com.waldev.sangatech.responses.work.PostWorkResponse
import com.waldev.sangatech.responses.work.UpdateWorkStateResponse
import com.waldev.sangatech.responses.work.WorkResponse
import com.waldev.sangatech.sealed.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class WorkRepoImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : WorkRepo, BaseApiResponse() {

    override fun addNewWorkRequest(workRequest: PostWorkRequest): Flow<NetworkResult<PostWorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.postNewWorkRequest(workRequest) })
        }.flowOn(Dispatchers.IO)

    override fun getCustomerPendingRequests(customerId: Int): Flow<NetworkResult<WorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getCustomerPendingRequests(customerId) })
        }.flowOn(Dispatchers.IO)

    override fun getCustomerRunningRequests(customerId: Int): Flow<NetworkResult<WorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getCustomerRunningRequests(customerId) })
        }.flowOn(Dispatchers.IO)

    override fun getCustomerRejectedRequests(customerId: Int): Flow<NetworkResult<WorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getCustomerRejectedRequests(customerId) })
        }.flowOn(Dispatchers.IO)

    override fun getCustomerCompletedRequests(customerId: Int): Flow<NetworkResult<WorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getCustomerCompletedRequests(customerId) })
        }.flowOn(Dispatchers.IO)

    override fun customerMarkRequestCompleted(
        customerId: Int,
        workId: Int
    ): Flow<NetworkResult<MarkCompleteWorkResponse>> = flow {
        emit(NetworkResult.Loading())
        emit(safeApiCall { remoteDataSource.customerMarkRequestAsCompleted(customerId, workId) })
    }.flowOn(Dispatchers.IO)

    override fun getWorkerRecentOrders(workerId: Int): Flow<NetworkResult<WorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getWorkerRecentOrders(workerId) })
        }.flowOn(Dispatchers.IO)

    override fun getWorkerPendingRequests(workerId: Int): Flow<NetworkResult<WorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getWorkerPendingRequests(workerId) })
        }.flowOn(Dispatchers.IO)

    override fun getWorkerRunningRequests(workerId: Int): Flow<NetworkResult<WorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getWorkerRunningRequests(workerId) })
        }.flowOn(Dispatchers.IO)

    override fun getWorkerReceivedRequests(workerId: Int): Flow<NetworkResult<WorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getWorkerReceivedRequests(workerId) })
        }.flowOn(Dispatchers.IO)

    override fun getWorkerCompletedRequests(workerId: Int): Flow<NetworkResult<WorkResponse>> =
        flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { remoteDataSource.getWorkerCompletedRequests(workerId) })
        }.flowOn(Dispatchers.IO)

    override fun workAcceptByWorkerRequest(
        workerId: Int,
        workId: Int
    ): Flow<NetworkResult<UpdateWorkStateResponse>> = flow {
        emit(NetworkResult.Loading())
        emit(safeApiCall { remoteDataSource.workerAcceptRequest(workerId, workId) })
    }.flowOn(Dispatchers.IO)

    override fun workRejectByWorkerRequest(
        workerId: Int,
        workId: Int
    ): Flow<NetworkResult<UpdateWorkStateResponse>> = flow {
        emit(NetworkResult.Loading())
        emit(safeApiCall { remoteDataSource.workerRejectRequest(workerId, workId) })
    }.flowOn(Dispatchers.IO)


}