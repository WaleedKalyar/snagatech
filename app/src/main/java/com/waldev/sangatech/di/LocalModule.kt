package com.waldev.sangatech.di

import android.content.Context
import com.waldev.sangatech.network.RemoteDataSource
import com.waldev.sangatech.repos.categories.CategoriesRepo
import com.waldev.sangatech.repos.categories.CategoriesRepoImpl
import com.waldev.sangatech.repos.chat.ChatRepo
import com.waldev.sangatech.repos.chat.ChatRepoImpl
import com.waldev.sangatech.repos.rating.RatingRepo
import com.waldev.sangatech.repos.rating.RatingRepoImpl
import com.waldev.sangatech.repos.users.UsersRepo
import com.waldev.sangatech.repos.users.UsersRepoImpl
import com.waldev.sangatech.repos.work.WorkRepo
import com.waldev.sangatech.repos.work.WorkRepoImpl
import com.waldev.sangatech.utils.AppDataStore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class LocalModule {
    @Provides
    @Singleton
    fun provideDataStore(@ApplicationContext context: Context): AppDataStore = AppDataStore(context)


    @Provides
    fun provideUsersRepo(remoteDataSource: RemoteDataSource): UsersRepo =
        UsersRepoImpl(remoteDataSource)

    @Provides
    fun provideCategoriesRepo(remoteDataSource: RemoteDataSource): CategoriesRepo =
        CategoriesRepoImpl(remoteDataSource)

    @Provides
    fun provideWorkRepo(remoteDataSource: RemoteDataSource): WorkRepo =
        WorkRepoImpl(remoteDataSource)

    @Provides
    fun provideChatRepo(remoteDataSource: RemoteDataSource): ChatRepo =
        ChatRepoImpl(remoteDataSource)

    @Provides
    fun provideRatingRepo(remoteDataSource: RemoteDataSource): RatingRepo =
        RatingRepoImpl(remoteDataSource)
}