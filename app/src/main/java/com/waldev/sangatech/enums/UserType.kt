package com.waldev.sangatech.enums

enum class UserType {
    WORKER, CUSTOMER
}