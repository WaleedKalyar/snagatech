package com.waldev.sangatech.enums

enum class MessageType {
    text, image, NONE
}