package com.waldev.sangatech.enums

enum class ResponseStatus {
    success, error, idle
}