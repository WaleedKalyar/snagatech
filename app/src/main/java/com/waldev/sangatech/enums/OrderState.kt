package com.waldev.sangatech.enums

enum class OrderState {
    RUNNING, REQUESTED, COMPLETED, REJECTED
}