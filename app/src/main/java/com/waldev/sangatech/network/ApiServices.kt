package com.waldev.sangatech.network

import com.waldev.sangatech.requests.PostMessageRequest
import com.waldev.sangatech.requests.PostWorkRequest
import com.waldev.sangatech.requests.RatingRequest
import com.waldev.sangatech.requests.SignUpRequest
import com.waldev.sangatech.responses.categories.CategoriesResponse
import com.waldev.sangatech.responses.categories.CategoryUsersResponse
import com.waldev.sangatech.responses.chat.PostMessageResponse
import com.waldev.sangatech.responses.rating.UploadRatingResponse
import com.waldev.sangatech.responses.rating.WorkerRatingsResponse
import com.waldev.sangatech.responses.user.ForgotPasswordResponse
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.responses.user.SignUpResponse
import com.waldev.sangatech.responses.user.UserDetailsResponse
import com.waldev.sangatech.responses.work.MarkCompleteWorkResponse
import com.waldev.sangatech.responses.work.PostWorkResponse
import com.waldev.sangatech.responses.work.UpdateWorkStateResponse
import com.waldev.sangatech.responses.work.WorkResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiServices {

    @POST("/api/v1/user/login")
    suspend fun loginWithEmailAndPassword(
        @Query("email") email: String,
        @Query("password") password: String
    ): Response<LoginResponse>

    @POST("/api/v1/user/registration")
    suspend fun registerNewUser(
        @Body request: SignUpRequest
    ): Response<SignUpResponse>


    @POST("/api/v1/update/user/profile")
    suspend fun updateUserProfile(
        @Body request: RequestBody
    ): Response<LoginResponse>
//UserUpdateResponse


    @POST("/api/v1/check/valid/email")
    suspend fun emailCodeForgotPassword(
        @Query("email") email: String
    ): Response<ForgotPasswordResponse>

    @Multipart
    @POST("/api/v1/update/user/profile")
    suspend fun updateUserProfile(
        @Part("id") id: Int,
        @Part("name") name: String?,
        @Part("email") email: String,
        @Part("password") password: String?,
        @Part("category") category: Int?,
        @Part("address_1") address1: String?,
        @Part("address_2") address2: String?,
        @Part("latitude") latitude: Double?,
        @Part("longitude") longitude: Double?,
        @Part profile_pic: MultipartBody.Part?,
        @Part("desc") desc: String?,
    ): Response<LoginResponse>


    @GET("/api/v1/user/data/{user_id}")
    suspend fun getWorkerDetailsById(
        @Path("user_id") userId: Int
    ): Response<UserDetailsResponse>

    //Categories
    @GET("/api/v1/all/category")
    suspend fun getAllCategories(): Response<CategoriesResponse>

    @GET("/api/v1/category/worker/{category_id}")
    suspend fun getAllUsersByCategory(
        @Path("category_id") CategoryId: Int
    ): Response<CategoryUsersResponse>

    //Work
    @POST("/api/v1/store/work/req")
    suspend fun addNewWorkRequest(
        @Body request: PostWorkRequest
    ): Response<PostWorkResponse>

    @GET("/api/v1/customer/pending/req/{customer_id}")
    suspend fun getCustomerPendingRequests(
        @Path("customer_id") customerId: Int
    ): Response<WorkResponse>

    @GET("api/v1/customer/rejected/req/{customer_id}")
    suspend fun getCustomerRejectedRequests(
        @Path("customer_id") customerId: Int
    ): Response<WorkResponse>

    @GET("/api/v1/customer/running/req/{customer_id}")
    suspend fun getCustomerRunningRequests(
        @Path("customer_id") customerId: Int
    ): Response<WorkResponse>

    @GET("/api/v1/customer/sent/req/{customer_id}")
    suspend fun getCustomerSentRequests(
        @Path("customer_id") customerId: Int
    ): Response<WorkResponse>

    @GET("/api/v1/customer/completed/req/{customer_id}")
    suspend fun getCustomerCompletedRequests(
        @Path("customer_id") customerId: Int
    ): Response<WorkResponse>

    @GET("/api/v1/cus/mark/req/complete/{request_id}/{customer_id}")
    suspend fun customerMarkRequestCompleted(
        @Path("customer_id") customerId: Int,
        @Path("request_id") requestId: Int
    ): Response<MarkCompleteWorkResponse>


    @GET("/api/v1/get/recent/completed-and-running/orders/{worker_id}")
    suspend fun getWorkerRecentOrders(
        @Path("worker_id") workerId: Int
    ): Response<WorkResponse>

    @GET("/api/v1/worker/pending/req/{worker_id}")
    suspend fun getWorkerPendingRequests(
        @Path("worker_id") workerId: Int
    ): Response<WorkResponse>

    @GET("/api/v1/worker/running/req/{worker_id}")
    suspend fun getWorkerRunningRequests(
        @Path("worker_id") workerId: Int
    ): Response<WorkResponse>

    @GET("/api/v1/worker/received/req/{worker_id}")
    suspend fun getWorkerReceivedRequests(
        @Path("worker_id") workerId: Int
    ): Response<WorkResponse>

    @GET("/api/v1/worker/completed/req/{worker_id}")
    suspend fun getWorkerCompletedRequests(
        @Path("worker_id") workerId: Int
    ): Response<WorkResponse>

    @GET("/api/v1/worker/accept/req/{request_id}/{worker_id}")
    suspend fun workerAcceptRequest(
        @Path("worker_id") workerId: Int,
        @Path("request_id") requestId: Int
    ): Response<UpdateWorkStateResponse>

    @GET("/api/v1/worker/reject/req/{request_id}/{worker_id}")
    suspend fun workerRejectRequest(
        @Path("worker_id") workerId: Int,
        @Path("request_id") requestId: Int
    ): Response<UpdateWorkStateResponse>

    //Rating----------------------------------

    @POST("/api/v1/rate/on/work/req")
    suspend fun rateOnWorkRequest(
        @Body request: RatingRequest
    ): Response<UploadRatingResponse>

    @GET("/api/v1/get/worker/ratings/{worker_id}")
    suspend fun getWorkerRatings(
        @Path("worker_id") workerId: Int
    ): Response<WorkerRatingsResponse>

    // Chat---------------------------------
    @POST("/api/v1/post/con/msg")
    suspend fun postNewMessage(
        @Body request: PostMessageRequest
    ): Response<PostMessageResponse>

}