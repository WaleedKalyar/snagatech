package com.waldev.sangatech.network

import com.waldev.sangatech.requests.PostMessageRequest
import com.waldev.sangatech.requests.PostWorkRequest
import com.waldev.sangatech.requests.RatingRequest
import com.waldev.sangatech.requests.SignUpRequest
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val apiServices: ApiServices
) {
    // Authentication & User Profile
    suspend fun loginUserWithEmailAndPassword(email: String, password: String) =
        apiServices.loginWithEmailAndPassword(email, password)

    suspend fun registerUserWithEmailAndPassword(
        signUpRequest: SignUpRequest
    ) = apiServices.registerNewUser(
        signUpRequest
    )

    suspend fun updateUserInfo(
        updateRequest: RequestBody
    ) = apiServices.updateUserProfile(updateRequest)

    suspend fun forgotPassword(email: String) = apiServices.emailCodeForgotPassword(email)

    suspend fun updateUserProfile(
        id: Int,
        name: String?,
        email: String,
        password: String?,
        category: Int?,
        address1: String?,
        address2: String?,
        latitude: Double?,
        longitude: Double?,
        profile_pic: MultipartBody.Part?,
        desc: String?
    ) = apiServices.updateUserProfile(
        id,
        name,
        email,
        password,
        category,
        address1,
        address2,
        latitude,
        longitude,
        profile_pic,
        desc
    )

    suspend fun getWorkerDetailsById(workerId: Int) =
        apiServices.getWorkerDetailsById(workerId)

    //Categories
    suspend fun getAllCategories() = apiServices.getAllCategories()

    suspend fun getAllUsersOfCategory(catId: Int) = apiServices.getAllUsersByCategory(catId)

    //Work
    suspend fun postNewWorkRequest(workRequest: PostWorkRequest) =
        apiServices.addNewWorkRequest(workRequest)

    //------------- Work customer requests-----------
    suspend fun getCustomerPendingRequests(customerId: Int) =
        apiServices.getCustomerSentRequests(customerId)

    suspend fun getCustomerRunningRequests(customerId: Int) =
        apiServices.getCustomerRunningRequests(customerId)

    suspend fun getCustomerSentRequests(customerId: Int) =
        apiServices.getCustomerSentRequests(customerId)

    suspend fun getCustomerCompletedRequests(customerId: Int) =
        apiServices.getCustomerCompletedRequests(customerId)

    suspend fun getCustomerRejectedRequests(customerId: Int) =
        apiServices.getCustomerRejectedRequests(customerId)

    suspend fun customerMarkRequestAsCompleted(customerId: Int, workId: Int) =
        apiServices.customerMarkRequestCompleted(customerId, workId)

    //--------------Work Worker requests----------

    suspend fun getWorkerRecentOrders(workerId: Int) =
        apiServices.getWorkerRecentOrders(workerId)

    suspend fun getWorkerPendingRequests(workerId: Int) =
        apiServices.getWorkerPendingRequests(workerId)

    suspend fun getWorkerRunningRequests(workerId: Int) =
        apiServices.getWorkerRunningRequests(workerId)

    suspend fun getWorkerReceivedRequests(workerId: Int) =
        apiServices.getWorkerReceivedRequests(workerId)

    suspend fun getWorkerCompletedRequests(workerId: Int) =
        apiServices.getWorkerCompletedRequests(workerId)

    suspend fun workerAcceptRequest(workerId: Int, requestId: Int) =
        apiServices.workerAcceptRequest(workerId, requestId)

    suspend fun workerRejectRequest(workerId: Int, requestId: Int) =
        apiServices.workerRejectRequest(workerId, requestId)


    //------------------------Rating----------------------
    suspend fun sendRatingFeedback(request: RatingRequest) =
        apiServices.rateOnWorkRequest(request)

    suspend fun getWorkerRatings(workerId: Int) =
        apiServices.getWorkerRatings(workerId)

    //-------------------------------CHAT ------------------------------
    suspend fun sendNewMessage(request: PostMessageRequest) =
        apiServices.postNewMessage(request)
}