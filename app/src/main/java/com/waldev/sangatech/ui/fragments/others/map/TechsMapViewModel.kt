package com.waldev.sangatech.ui.fragments.others.map

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.repos.categories.CategoriesRepo
import com.waldev.sangatech.responses.categories.CategoryUsersResponse
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.sealed.NetworkResult.Success
import com.waldev.sangatech.utils.AppDataStore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class TechsMapViewModel @Inject constructor(
    private val categoriesRepo: CategoriesRepo,
    private val appDataStore: AppDataStore
) : ViewModel() {

    private val _techsListState =
        MutableSharedFlow<MutableList<CategoryUsersResponse.Data.CatUser>>()
    val techsListState = _techsListState.asSharedFlow()

    private val _currentUserState =
        MutableStateFlow<LoginResponse.Data.UserDetail>(LoginResponse.Data.UserDetail())
    val currentUserState = _currentUserState.asStateFlow()

    init {
        viewModelScope.launch {
            appDataStore.getUserFromDataStore().collectLatest {
                _currentUserState.value = it
            }
        }
    }

    fun getAllTechs() {
        viewModelScope.launch {
            categoriesRepo.getAllCategories().collectLatest {
                val techsList: MutableList<CategoryUsersResponse.Data.CatUser> = ArrayList()
                if (it is Success) {
                    if (it.result != null)
                        for (cat in it.result.data.catDetail) {
                            Timber.d("Tech list category found with id -> ${cat.id}")
                            categoriesRepo.getAllUsersByCategory(cat.id)
                                .collectLatest { it2 ->
                                    if (it2.result != null && it2.result.data.catUsers.isNotEmpty()) {
                                        Timber.d("Tech list tech found with cat -> ${cat.id} techsSize -> ${it2.result.data.catUsers.size}")
                                        techsList.addAll(it2.result.data.catUsers)
                                        _techsListState.emit(techsList)
                                        Timber.d("Tech list size after fetch -> ${_techsListState.asLiveData().value?.size}")
                                    }

                                }

                        }

                }
            }

        }
    }

    fun getAllTechs2() {
        viewModelScope.launch {
            categoriesRepo.getAllCategories()
                .map {

                    if (it.result != null && it.result.data.catDetail.isNotEmpty()) {
                        for (cat in it.result.data.catDetail) {
                            categoriesRepo.getAllUsersByCategory(cat.id)
                        }
                    }

                }
                .collectLatest { }
        }
    }
}