package com.waldev.sangatech.ui.fragments.others.chat

import android.content.res.ColorStateList
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.ChatFragmentBinding
import com.waldev.sangatech.enums.MessageType
import com.waldev.sangatech.ui.fragments.others.chat.adapters.ChatAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChatFragment : Fragment() {
    private var _binding: ChatFragmentBinding? = null
    private val binding get() = _binding!!

    companion object {
        fun newInstance() = ChatFragment()
    }

    private val viewModel: ChatViewModel by viewModels()

    private lateinit var adapter: ChatAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ChatFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initTypingListener()
        initClickListeners()
    }

    private fun initClickListeners() {
        binding.btnSend.setOnClickListener {
                val message = binding.edInputMessage.text.toString()
            if (message.isNotEmpty()){
                viewModel.sendMessage(message,MessageType.text,null)
            }
        }
    }

    private fun initTypingListener() {
        binding.edInputMessage.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (text.isNullOrEmpty()) {
                    binding.btnSend.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(),R.color.colorRed))
                }else{
                    binding.btnSend.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(),R.color.colorGray))
                }
            }

            override fun afterTextChanged(p0: Editable?) {
            }

        })
    }

    private fun initAdapter() {
        adapter = ChatAdapter(
            viewModel.currentUserState.value.id,
        ) { model, view ->

        }
        binding.recyclerChat.adapter = adapter
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}