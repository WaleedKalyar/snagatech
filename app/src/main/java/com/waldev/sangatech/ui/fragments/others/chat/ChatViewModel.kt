package com.waldev.sangatech.ui.fragments.others.chat

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.enums.MessageType
import com.waldev.sangatech.repos.chat.ChatRepo
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.utils.AppDataStore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

@HiltViewModel
class ChatViewModel @Inject constructor(
    private val appDataStore: AppDataStore,
    private val chatRepo: ChatRepo
) : ViewModel() {

    private val _currentUserState =
        MutableStateFlow<LoginResponse.Data.UserDetail>(LoginResponse.Data.UserDetail())
    val currentUserState = _currentUserState.asStateFlow()

    init {
        viewModelScope.launch {
            appDataStore.getUserFromDataStore().collectLatest {
                _currentUserState.value = it
            }
        }
    }


    fun sendMessage(message: String?, text: MessageType, file: File?) {
        val reqFile: RequestBody? = file?.asRequestBody("image/*".toMediaTypeOrNull())
        var multipartBody: MultipartBody.Part? = null
        if (reqFile != null) {
            multipartBody =
                MultipartBody.Part.createFormData("profile_pic", file.name, reqFile)
        }

//        val request = PostMessageRequest(
//            conversation_id = ,
//            sender_id = _currentUserState.value.id,
//            receiver_id = ,
//            msg = message,
//            file = file.name,
//            file_url =
//
//        )
        //chatRepo.sendNewMessage(request =)
    }
}