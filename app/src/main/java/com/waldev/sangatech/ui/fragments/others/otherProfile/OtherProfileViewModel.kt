package com.waldev.sangatech.ui.fragments.others.otherProfile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.repos.rating.RatingRepo
import com.waldev.sangatech.repos.users.UsersRepo
import com.waldev.sangatech.responses.rating.WorkerRatingsResponse
import com.waldev.sangatech.responses.user.UserDetailsResponse
import com.waldev.sangatech.sealed.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OtherProfileViewModel @Inject constructor(
    private val usersRepo: UsersRepo,
    private val ratingRepo: RatingRepo
) : ViewModel() {
    private val _userDetails =
        MutableStateFlow<NetworkResult<UserDetailsResponse>>(NetworkResult.Idle())
    val userDetails = _userDetails.asStateFlow()

    private val _workerFeedbacksState =
        MutableStateFlow<NetworkResult<WorkerRatingsResponse>>(NetworkResult.Idle())
    val workerFeedbacksState = _workerFeedbacksState.asStateFlow()

    var userId: Int = 0

    fun getUserDetails() {
        viewModelScope.launch {
            usersRepo.getWorkerDetailsId(userId)
                .collectLatest {
                    _userDetails.value = it
                }
        }
    }

    fun getWorkerFeedbacks(workerId: Int) {
        viewModelScope.launch {
            ratingRepo.getRatingsByWorkerId(workerId).collectLatest {
                _workerFeedbacksState.value = it
            }
        }
    }
}