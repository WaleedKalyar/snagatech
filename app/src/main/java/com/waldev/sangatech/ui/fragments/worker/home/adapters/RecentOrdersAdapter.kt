package com.waldev.sangatech.ui.fragments.worker.home.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.ItemOrderBinding
import com.waldev.sangatech.enums.UserType
import com.waldev.sangatech.responses.work.WorkResponse
import com.waldev.sangatech.utils.DateUtilities
import java.util.*

class RecentOrdersAdapter(
    private val userType: UserType,
) :
    ListAdapter<WorkResponse.Data.WorkReq, RecentOrdersAdapter.OrderViewHolder>(DiffCallback) {

    inner class OrderViewHolder(private val binding: ItemOrderBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(model: WorkResponse.Data.WorkReq) {
            binding.apply {
                tvTitle.text = model.title
                tvAmount.text = String.format("$%.2f", model.amount)
                if (userType == UserType.WORKER) {
                    binding.titleOrderBy.text = "Order By"
                    tvUserName.text = model.customer_detail.name
                } else {
                    binding.titleOrderBy.text = "Order To"
                    tvUserName.text = model.worker_detail.name
                }

                tvId.text = "S2OI-${Calendar.getInstance().get(Calendar.YEAR)}-${model.id}"

                tvDate.text = DateUtilities.getDateFromMillis(model.arr_time.toLong())
                tvTime.text = DateUtilities.getTimeFromMillis(model.arr_time.toLong())
                when (model.status) {
                    1 -> {
                        chipStatus.setChipBackgroundColorResource(R.color.colorBlue)
                        rlTop.setBackgroundColor(ContextCompat.getColor(root.context,R.color.colorBlue))
                        chipStatus.text = "Running"
                    }
                    2 -> {
                        chipStatus.setChipBackgroundColorResource(R.color.colorYellow)
                        rlTop.setBackgroundColor(ContextCompat.getColor(root.context,R.color.colorYellow))
                        if (userType == UserType.CUSTOMER) {
                            chipStatus.text = "Sent"
                        } else {
                            chipStatus.text = "Pending"
                        }

                    }
                    3 -> {
                        chipStatus.setChipBackgroundColorResource(R.color.colorGreen)
                        rlTop.setBackgroundColor(ContextCompat.getColor(root.context,R.color.colorGreen))
                        chipStatus.text = "Completed"
                    }
                    0 -> {
                        chipStatus.setChipBackgroundColorResource(R.color.colorRed)
                        rlTop.setBackgroundColor(ContextCompat.getColor(root.context,R.color.colorRed))
                        chipStatus.text = "Rejected"
                    }
                }

            }
        }

    }


    object DiffCallback : DiffUtil.ItemCallback<WorkResponse.Data.WorkReq>() {

        override fun areItemsTheSame(
            oldItem: WorkResponse.Data.WorkReq,
            newItem: WorkResponse.Data.WorkReq
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: WorkResponse.Data.WorkReq,
            newItem: WorkResponse.Data.WorkReq
        ): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        return OrderViewHolder(
            ItemOrderBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}