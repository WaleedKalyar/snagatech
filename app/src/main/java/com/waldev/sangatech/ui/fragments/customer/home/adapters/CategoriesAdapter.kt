package com.waldev.sangatech.ui.fragments.customer.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.waldev.sangatech.databinding.ItemCategoryBinding
import com.waldev.sangatech.responses.categories.CategoriesResponse

class CategoriesAdapter(
    private val onCategoryClick: (CategoriesResponse.Data.CatDetail) -> Unit,
) :
    ListAdapter<CategoriesResponse.Data.CatDetail, CategoriesAdapter.CategoryViewHolder>(
        DiffCallback
    ) {

    inner class CategoryViewHolder(private val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: CategoriesResponse.Data.CatDetail) {
            binding.apply {
                tvName.text = model.name
                if (model.thumbnail_url != null && model.thumbnail_url!!.isNotEmpty()) {
                    img.load(model.thumbnail_url)
                }
                root.setOnClickListener {
                    onCategoryClick(model)
                }
            }

        }
    }

    object DiffCallback : DiffUtil.ItemCallback<CategoriesResponse.Data.CatDetail>() {

        override fun areItemsTheSame(
            oldItem: CategoriesResponse.Data.CatDetail,
            newItem: CategoriesResponse.Data.CatDetail
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: CategoriesResponse.Data.CatDetail,
            newItem: CategoriesResponse.Data.CatDetail
        ): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}