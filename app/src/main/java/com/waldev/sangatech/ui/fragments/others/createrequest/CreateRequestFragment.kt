package com.waldev.sangatech.ui.fragments.others.createrequest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.CreateRequestFragmentBinding
import com.waldev.sangatech.requests.PostWorkRequest
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.utils.AppConsts.CATEGORY_ID
import com.waldev.sangatech.utils.AppConsts.WORKER_ID
import com.waldev.sangatech.utils.AppConsts.WORKER_NAME
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import com.waldev.sangatech.utils.ExtensionUtils.hideWaitingDialogIfVisible
import com.waldev.sangatech.utils.ExtensionUtils.showOrUpdateWaitingDialog
import com.waldev.sangatech.utils.ToastUtility
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.util.*


@AndroidEntryPoint
class CreateRequestFragment : Fragment() {
    private var _binding: CreateRequestFragmentBinding? = null
    private val binding get() = _binding!!


    private val viewModel: CreateRequestViewModel by viewModels()
    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = CreateRequestFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initArguments()
        initDaysSpinner()
        initMonthsSpinner()
        initYearsSpinner()
        setClickListeners()
        initCollectors()
        initSpinnerListeners()
        setSelections()
    }

    private fun initArguments() {
        viewModel.workerId = requireArguments().getInt(WORKER_ID, 0)
        viewModel.categoryId = requireArguments().getInt(CATEGORY_ID, 0)
        viewModel.workerName = requireArguments().getString(WORKER_NAME, "")
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.uploadWorkState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    Timber.d("Error -> ${it.message}")
                    ToastUtility.errorToast(requireContext(), it.message)
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Adding request")
                }
                is NetworkResult.Success -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    if (it.result != null) {
                        ToastUtility.successToast(requireContext(), it.result.status.name)
                        findNavController().navigateUp()
                    } else {
                        ToastUtility.errorToast(requireContext(), "Something went wrong")
                    }
                }
            }
        }
    }

    private fun setClickListeners() {
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.btnProceed.setOnClickListener {
            validateInputs()
        }

        binding.btnCalendar.setOnClickListener { pickDateByCalendar() }
    }

    private fun validateInputs() {
        binding.apply {
            val title = etTitle.text.toString()
            val stAmount = etAmount.text.toString()
            val address = etAddress.text.toString()
            val description = etDesc.text.toString()

            if (title.isEmpty()) {
                inputTitle.error = "Please enter title"
            } else if (stAmount.isEmpty()) {
                inputAmount.error = "Please enter amount"
            } else if (stAmount.toDouble() <= 0) {
                inputAmount.error = "Amount is invalid"
            } else if (address.isEmpty()) {
                inputAddress.error = "Please add your address"
            } else if (description.isEmpty() || description.length < 120) {
                inputDesc.error = "Please enter description at lease 120 characters"
            }
//            else if (viewModel.calendar.timeInMillis) {
//                ToastUtility.errorToast(requireContext(), "Please chose date and time")
            //  }
            else {
                inputTitle.error = null
                inputAmount.error = null
                inputAddress.error = null
                inputDesc.error = null
                val request = PostWorkRequest(
                    id = null,
                    customer_id = viewModel.userDetails.value.id,
                    worker_id = viewModel.workerId,
                    cat_id = viewModel.categoryId,
                    title = title,
                    amount = stAmount.toDouble(),
                    address = address,
                    desc = description,
                    arrival_time = viewModel.calendar.timeInMillis.toString()
                )
                Timber.d("Work request is -> ${request.toString()}")
                viewModel.postNewWorkRequest(request)
            }
        }
    }


    private fun initDaysSpinner() {
        val daysList: MutableList<String> = ArrayList()
        daysList.add("Days")
        for (i in 1..31) {
            daysList.add(i.toString())
        }
        val adapter = ArrayAdapter(requireContext(), R.layout.spinner_item, daysList)
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        binding.spDays.adapter = adapter
    }

    private fun initMonthsSpinner() {
        val monthsList: MutableList<String> = ArrayList()
        monthsList.add("Months")
        for (i in 1..12) {
            monthsList.add(i.toString())
        }
        val adapter = ArrayAdapter(requireContext(), R.layout.spinner_item, monthsList)
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        binding.spMonths.adapter = adapter
    }

    private fun initYearsSpinner() {
        val yearsList: MutableList<String> = ArrayList()
        yearsList.add("Year")
        for (i in 2022..2030) {
            yearsList.add(i.toString())
        }
        val adapter = ArrayAdapter(requireContext(), R.layout.spinner_item, yearsList)
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        binding.spYear.adapter = adapter
    }

    private fun initSpinnerListeners() {
        binding.spDays.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    p3: Long
                ) {
                    if (position > 0) {
                        val selection = parent?.getItemAtPosition(position).toString()
                        viewModel.calendar.set(Calendar.DAY_OF_MONTH, selection.toInt())
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }
            }

        binding.spMonths.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    p3: Long
                ) {
                    if (position > 0) {
                        val selection = parent?.getItemAtPosition(position).toString()
                        viewModel.calendar.set(Calendar.MONTH, selection.toInt())
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }
            }
        binding.spYear.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    p3: Long
                ) {
                    if (position > 0) {
                        val selection = parent?.getItemAtPosition(position).toString()
                        viewModel.calendar.set(Calendar.YEAR, selection.toInt())
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }
            }
    }


    private fun pickDateByCalendar() {
        val constraints = CalendarConstraints.Builder()
            .setOpenAt(System.currentTimeMillis())
            .setValidator(DateValidatorPointForward.now())
            .build()
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setCalendarConstraints(constraints)
                .setTitleText("Select date")
                .build()

        datePicker.addOnPositiveButtonClickListener {
            viewModel.calendar.timeInMillis = it
            setSelections()
        }
        datePicker.show(childFragmentManager, "D_PICKER")

    }

    private fun setSelections() {
        binding.spDays.setSelection(viewModel.calendar.get(Calendar.DAY_OF_MONTH))
        binding.spMonths.setSelection(viewModel.calendar.get(Calendar.MONTH) + 1)
        val yearSelection = viewModel.calendar.get(Calendar.YEAR) - 2021
        binding.spYear.setSelection(yearSelection)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}