package com.waldev.sangatech.ui.fragments.others.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.TechsMapFragmentBinding
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class TechsMapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var binding: TechsMapFragmentBinding

    //private val binding = _binding!!
    private var mMap: GoogleMap? = null

    companion object {
        fun newInstance() = TechsMapFragment()
    }

    private val viewModel: TechsMapViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = TechsMapFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMapFragment()
        initClickListeners()
        initCollectors()
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.techsListState) {
            Timber.d("Techs list -> ${it.size} techs found")
            mMap?.let { mMap ->
                mMap.clear()
                for (worker in it) {

                    mMap.addMarker(
                        MarkerOptions().position(
                            LatLng(
                                worker.latitude.toDouble(),
                                worker.longitude.toDouble()
                            )
                        ).title(worker.name)
                    )
                }
            }
        }
    }

    private fun initClickListeners() {
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun initMapFragment() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

//    override fun onDestroyView() {
//        _binding = null
//        super.onDestroyView()
//    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap?.let {
            runCatching {
                val success = it.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                        requireContext(), R.raw.map_style
                    )
                )
                success
            }.onSuccess {
                Timber.d("Map: Style applied")
            }.onFailure {
                Timber.d("Map: Style applied failed error -> ${it.localizedMessage}")
            }

        }

        // Add a marker in Sydney and move the camera
//        val sydney = LatLng(37.34, 72.32)
//        mMap.addMarker(MarkerOptions().position(sydney).title("TEST LOCATION"))
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14f))
//        mMap?.addCircle(
//            CircleOptions()
//                .center(
//                    LatLng(
//                        viewModel.currentUserState.value.latitude.toDouble(),
//                        viewModel.currentUserState.value.longitude.toDouble()
//                    )
//                )
//                .radius(24.00)
//                .visible(true)
        //)
        viewModel.getAllTechs()
    }
}