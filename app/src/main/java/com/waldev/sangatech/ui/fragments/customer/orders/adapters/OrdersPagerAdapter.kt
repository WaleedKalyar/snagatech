package com.waldev.sangatech.ui.fragments.customer.orders.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.waldev.sangatech.enums.OrderState
import com.waldev.sangatech.ui.fragments.customer.orders.page.OrderPageFragment

class OrdersPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    private val fragmentList: MutableList<Fragment> = ArrayList()

    init {
        fragmentList.add(OrderPageFragment.instance(OrderState.RUNNING))
        fragmentList.add(OrderPageFragment.instance(OrderState.REQUESTED))
        fragmentList.add(OrderPageFragment.instance(OrderState.COMPLETED))
        fragmentList.add(OrderPageFragment.instance(OrderState.REJECTED))
    }

    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }
}