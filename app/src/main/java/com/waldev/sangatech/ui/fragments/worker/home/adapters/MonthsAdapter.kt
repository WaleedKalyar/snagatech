package com.waldev.sangatech.ui.fragments.worker.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.waldev.sangatech.databinding.ItemMonthBinding
import com.waldev.sangatech.models.MonthModel

class MonthsAdapter : ListAdapter<MonthModel, MonthsAdapter.MonthViewHolder>(DiffCallback) {

    inner class MonthViewHolder(private val binding: ItemMonthBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: MonthModel) {
            binding.apply {
                tvMonth.text = model.month
                tvEarns.text = String.format("$%.2f", model.earnings)
            }
        }
    }

    object DiffCallback : DiffUtil.ItemCallback<MonthModel>() {

        override fun areItemsTheSame(
            oldItem: MonthModel,
            newItem: MonthModel
        ): Boolean {
            return oldItem.month == newItem.month
        }

        override fun areContentsTheSame(
            oldItem: MonthModel,
            newItem: MonthModel
        ): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MonthViewHolder {
        return MonthViewHolder(
            ItemMonthBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MonthViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}