package com.waldev.sangatech.ui.fragments.others.otherProfile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import coil.load
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.OtherProfileFragmentBinding
import com.waldev.sangatech.enums.ResponseStatus
import com.waldev.sangatech.responses.user.UserDetailsResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.ui.fragments.customer.profile.adapters.FeedbacksAdapter
import com.waldev.sangatech.utils.AppConsts
import com.waldev.sangatech.utils.AppConsts.USER_ID
import com.waldev.sangatech.utils.CenterZoomLayoutManager
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import com.waldev.sangatech.utils.ExtensionUtils.hideWaitingDialogIfVisible
import com.waldev.sangatech.utils.ExtensionUtils.showOrUpdateWaitingDialog
import com.waldev.sangatech.utils.ToastUtility
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class OtherProfileFragment : Fragment() {
    private var _binding: OtherProfileFragmentBinding? = null
    private val binding get() = _binding!!

    companion object {
        fun newInstance() = OtherProfileFragment()
    }

    private val viewModel: OtherProfileViewModel by viewModels()
    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }
    private val adapter: FeedbacksAdapter by lazy { FeedbacksAdapter() }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = OtherProfileFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initArguments()
        initCollectors()
        initClickListeners()
    }

    private fun initClickListeners() {
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.userDetails) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    Timber.d("WorkerDetails -> Error ${it.message}")
                    ToastUtility.errorToast(
                        requireContext(),
                        "Something went wrong! please try again"
                    )
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Fetching Info")
                }
                is NetworkResult.Success -> {
                    Timber.d("WorkerDetails : Response -> ${it.result?.status}")
                    hideWaitingDialogIfVisible(waitingDialog)
                    if (it.result != null) {
                        setDetailsInfo(it.result.data.catUsers)
                    } else {
                        ToastUtility.warningToast(
                            requireContext(),
                            "User Details data not available"
                        )
                    }
                }
            }
        }
        collectLatestLifecycleFlow(viewModel.workerFeedbacksState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    ToastUtility.errorToast(requireContext(), it.message)
                    Timber.d("Error -> ${it.message}")
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Fetching Feedbacks")
                }
                is NetworkResult.Success -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    it.result?.let { result ->
                        Timber.d("Success : resp -> ${result.toString()}")
                        if (result.status == ResponseStatus.success) {
                            adapter.submitList(result.data.ratings)
                        } else {
                            ToastUtility.warningToast(
                                requireContext(),
                                "Something went wrong!"
                            )
                        }
                    }


                }
            }
        }
    }

    private fun setDetailsInfo(userDetail: UserDetailsResponse.Data.CatUsers) {
        binding.apply {
            // Timber.d("ProfilePage: UserId -> ${userDetail.} Profile Url -> ${userDetail.profile_url} Profile Image -> ${userDetail.profile_img}")
            if (userDetail.profile_url != null && userDetail.profile_url!!.isNotEmpty()) {
                imageProfile.load(userDetail.profile_url)
            } else {
                imageProfile.setImageResource(R.drawable.ic_profile)
            }
            tvName.text = userDetail.name
            tvDescription.text = userDetail.desc
            tvAddress.text = userDetail.address_1

            if (userDetail.type == AppConsts.USER_TYPE_WORKER) {
                tvFeedbacks.visibility = View.VISIBLE
                feedbackRecycler.visibility = View.VISIBLE
                tvRating.visibility = View.VISIBLE
                initFeedbacksInfo(viewModel.userId)
            } else {
                tvFeedbacks.visibility = View.GONE
                feedbackRecycler.visibility = View.GONE
                tvRating.visibility = View.GONE
            }
        }
    }

    private fun initFeedbacksInfo(workerId: Int) {
        val layoutManager = CenterZoomLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.feedbackRecycler.layoutManager = layoutManager
        val pagerSnapHelper = PagerSnapHelper()
        binding.feedbackRecycler.onFlingListener = null
        pagerSnapHelper.attachToRecyclerView(binding.feedbackRecycler)
        binding.feedbackRecycler.adapter = adapter

        viewModel.getWorkerFeedbacks(workerId)
    }

    private fun initArguments() {
        viewModel.userId = requireArguments().getInt(USER_ID, 0)
        viewModel.getUserDetails()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}