package com.waldev.sangatech.ui.fragments.worker.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.repos.users.UsersRepo
import com.waldev.sangatech.repos.work.WorkRepo
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.responses.user.UserDetailsResponse
import com.waldev.sangatech.responses.work.WorkResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.utils.AppDataStore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SellerHomeViewModel @Inject constructor(
    private val appDataStore: AppDataStore,
    private val workRepo: WorkRepo,
    private val usersRepo: UsersRepo
) : ViewModel() {

    private val _userinfo = MutableStateFlow(LoginResponse.Data.UserDetail())
    val userInfo = _userinfo.asStateFlow()

    private val _userDetails =
        MutableStateFlow<NetworkResult<UserDetailsResponse>>(NetworkResult.Idle())

    val userDetails = _userDetails.asStateFlow()

    private val _recentWorks = MutableStateFlow<NetworkResult<WorkResponse>>(NetworkResult.Idle())
    val recentWorks = _recentWorks.asStateFlow()


    init {
        getLocalUserDetails()
    }

    private fun getRecentOrders() {
        viewModelScope.launch {
            workRepo.getWorkerRecentOrders(userInfo.value.id)
                .collectLatest {
                    _recentWorks.value = it
                }
        }
    }

    private fun getUserDetails() {
        //Timber.d("SellerVM: luid -> ${userInfo.value.id}")
        viewModelScope.launch {
            usersRepo.getWorkerDetailsId(userInfo.value.id)
                .collectLatest {
                    _userDetails.value = it
                }
        }
    }

    private fun getLocalUserDetails() {
        viewModelScope.launch {
            appDataStore.getUserFromDataStore().collectLatest {
                _userinfo.value = it
                getUserDetails()
                getRecentOrders()
            }
        }
    }
}