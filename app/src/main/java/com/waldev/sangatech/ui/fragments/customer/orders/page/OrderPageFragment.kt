package com.waldev.sangatech.ui.fragments.customer.orders.page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.waldev.sangatech.databinding.FragmentOrderPageBinding
import com.waldev.sangatech.enums.OrderState
import com.waldev.sangatech.enums.ResponseStatus
import com.waldev.sangatech.enums.UserType
import com.waldev.sangatech.responses.work.MarkCompleteWorkResponse
import com.waldev.sangatech.responses.work.WorkResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.dialogs.feedback.FeedbackDialog
import com.waldev.sangatech.ui.dialogs.mark.MarkAsCompleteDialog
import com.waldev.sangatech.ui.dialogs.orderdecision.OrderDecisionDialog
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.ui.fragments.customer.orders.adapters.OrdersAdapter
import com.waldev.sangatech.utils.AppConsts.USER_TYPE_WORKER
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import com.waldev.sangatech.utils.ExtensionUtils.hideWaitingDialogIfVisible
import com.waldev.sangatech.utils.ExtensionUtils.showOrUpdateWaitingDialog
import com.waldev.sangatech.utils.ToastUtility
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class OrderPageFragment() : Fragment() {
    private var _binding: FragmentOrderPageBinding? = null
    private val binding get() = _binding!!
    private val state: OrderState by lazy {
        OrderState.valueOf(requireArguments().getString(ORDER_STATE, "RUNNING"))
    }
    private val viewModel: OrderPageViewModel by lazy { ViewModelProvider(this)[state.name, OrderPageViewModel::class.java] }

    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }

    private lateinit var adapter: OrdersAdapter
    var userType = UserType.CUSTOMER

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentOrderPageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("OrderPage: State -> $state")
        initAdapter()
        initCollectors()
        initRequests()
        initClickListeners()
    }

    private fun initClickListeners() {
        binding.btnRetry.setOnClickListener {
            initRequests()
        }
    }

    override fun onResume() {
        super.onResume()
        initRequests()
    }

    private fun initRequests() {
        if (userType == UserType.WORKER) {
            Timber.d("OrderPage: Worker is requesting Id -> ${viewModel.userDetails.value.id}")
            viewModel.getOrdersForWorker(state, viewModel.userDetails.value.id)
        } else {
            Timber.d("OrderPage: Customer is requesting Id -> ${viewModel.userDetails.value.id}")
            viewModel.getOrdersForCustomer(state, viewModel.userDetails.value.id)
        }
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.ordersState) {
            when (it) {
                is NetworkResult.Error -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.VISIBLE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerOrders.visibility = View.GONE
                    }
                }
                is NetworkResult.Idle -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerOrders.visibility = View.GONE
                    }
                }
                is NetworkResult.Loading -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.startShimmer()
                        shimmerLayout.visibility = View.VISIBLE
                        recyclerOrders.visibility = View.GONE
                    }
                }
                is NetworkResult.Success -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerOrders.visibility = View.GONE
                    }
                    Timber.d("OrderPage : Response -> ${it.result?.status}")
                    if (it.result != null && it.result.data.workReq.isNotEmpty()) {
                        binding.recyclerOrders.visibility = View.VISIBLE
                        adapter.submitList(it.result.data.workReq)
                    } else {
                        binding.rlNoData.visibility = View.VISIBLE
                    }
                }
            }
        }
        collectLatestLifecycleFlow(viewModel.acceptOrdersState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    Timber.d("OrderPage -> Error ${it.message}")
                    ToastUtility.errorToast(
                        requireContext(),
                        "Something went wrong! please try again"
                    )
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Accepting")
                }
                is NetworkResult.Success -> {
                    Timber.d("OrderPage : Response -> ${it.result?.status}")
                    hideWaitingDialogIfVisible(waitingDialog)
                    if (it.result != null) {
                        ToastUtility.successToast(requireContext(), "Offer accepted successfully")
                        viewModel.getOrdersForWorker(state, viewModel.userDetails.value.id)
                    } else {
                        ToastUtility.warningToast(requireContext(), "Data not available")
                    }
                }
            }
        }
        collectLatestLifecycleFlow(viewModel.rejectOrdersState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    Timber.d("OrderPage -> Error ${it.message}")
                    ToastUtility.errorToast(
                        requireContext(),
                        "Something went wrong! please try again"
                    )
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Rejecting")
                }
                is NetworkResult.Success -> {
                    Timber.d("OrderPage : Response -> ${it.result?.status}")
                    hideWaitingDialogIfVisible(waitingDialog)
                    if (it.result != null) {
                        ToastUtility.successToast(requireContext(), "Offer rejected successfully")
                        viewModel.getOrdersForWorker(state, viewModel.userDetails.value.id)
                    } else {
                        ToastUtility.warningToast(requireContext(), "Data not available")
                    }
                }
            }
        }
        collectLatestLifecycleFlow(viewModel.markCompleteOrdersState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    Timber.d("OrderPage -> Error ${it.message}")
                    ToastUtility.errorToast(
                        requireContext(),
                        "Something went wrong! please try again"
                    )
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Marking")
                }
                is NetworkResult.Success -> {
                    Timber.d("OrderPage : Response code -> ${it.result?.status}")
                    Timber.d("OrderPage : Response message -> ${it.result?.message}")
                    hideWaitingDialogIfVisible(waitingDialog)
                    if (it.result != null && it.result.status == ResponseStatus.success) {
                        ToastUtility.successToast(
                            requireContext(),
                            "Offer marked complete successfully"
                        )
                        viewModel.getOrdersForCustomer(state, viewModel.userDetails.value.id)
                        showRatingDialog(it.result.data.workReq)
                    } else {
                        ToastUtility.warningToast(requireContext(), "Data not available")
                        Timber.d("OrderPage: Mark complete ->  Response is success but has empty body")
                    }
                }
            }
        }
        collectLatestLifecycleFlow(viewModel.sendFeedbackState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    Timber.d("OrderPage -> Error ${it.message}")
                    ToastUtility.errorToast(
                        requireContext(),
                        "Something went wrong! please try again"
                    )
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Sending Feedback")
                }
                is NetworkResult.Success -> {
                    Timber.d("OrderPage : Response -> ${it.result?.status}")
                    hideWaitingDialogIfVisible(waitingDialog)
                    if (it.result != null) {
                        ToastUtility.successToast(requireContext(), "Feedback sent successfully")
                    } else {
                        ToastUtility.warningToast(
                            requireContext(),
                            "Feedback sent by something wrong detected"
                        )
                    }
                }
            }
        }
    }

    private fun showRatingDialog(order: MarkCompleteWorkResponse.Data.WorkReq) {
        Timber.d("OrderPage mark complete model -> ${order.toString()}")
        FeedbackDialog(
            context = requireContext(),
            order = order
        ) {
            Timber.d("OrderPage: feedback is -> ${it.toString()}")
            viewModel.sendFeedbackToWorker(it)
        }.show()
    }

    private fun initAdapter() {
        userType = UserType.CUSTOMER
        if (viewModel.userDetails.value.type == USER_TYPE_WORKER) {
            userType = UserType.WORKER
        }

        adapter = OrdersAdapter(state, userType) { order, userType ->
            if (state == OrderState.REQUESTED && userType == UserType.WORKER) {
                showAcceptRejectDialog(order, userType)
            }
            if (state == OrderState.RUNNING && userType == UserType.CUSTOMER) {
                showMarkCompleteDialog(order, userType)
            }
        }
        binding.recyclerOrders.adapter = adapter
    }

    private fun showMarkCompleteDialog(order: WorkResponse.Data.WorkReq, userType: UserType) {
        Timber.d("OrderPage: Mark complete -> customer id -> ${viewModel.userDetails.value.id} work id -> ${order.id}")
        MarkAsCompleteDialog(
            context = requireContext(),
            order = order,
            userType = userType,
        ) {
            Timber.d("OrderPage: Mark complete -> customer id -> ${viewModel.userDetails.value.id} work id -> ${order.id}")
            viewModel.onOrderMarkAsComplete(viewModel.userDetails.value.id, order.id)
        }
            .show()
    }

    private fun showAcceptRejectDialog(order: WorkResponse.Data.WorkReq, userType: UserType) {
        OrderDecisionDialog(
            context = requireContext(),
            order = order,
            userType = userType,
            onOrderAccept = {
                viewModel.acceptOffer(viewModel.userDetails.value.id, order.id)
            },
            onOrderReject = {
                viewModel.rejectOffer(viewModel.userDetails.value.id, order.id)
            })
            .show()
    }


    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }


    companion object {
        private const val ORDER_STATE = "state"
        fun instance(state: OrderState): OrderPageFragment {
            val bundle = Bundle()
            bundle.putString(ORDER_STATE, state.name)
            val frag = OrderPageFragment()
            return frag.apply {
                arguments = bundle
            }
        }
    }


}