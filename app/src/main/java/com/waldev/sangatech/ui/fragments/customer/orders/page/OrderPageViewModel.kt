package com.waldev.sangatech.ui.fragments.customer.orders.page

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.enums.OrderState
import com.waldev.sangatech.repos.rating.RatingRepo
import com.waldev.sangatech.repos.work.WorkRepo
import com.waldev.sangatech.requests.RatingRequest
import com.waldev.sangatech.responses.rating.UploadRatingResponse
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.responses.work.MarkCompleteWorkResponse
import com.waldev.sangatech.responses.work.UpdateWorkStateResponse
import com.waldev.sangatech.responses.work.WorkResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.utils.AppDataStore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OrderPageViewModel @Inject constructor(
    private val appDataStore: AppDataStore,
    private val workRepo: WorkRepo,
    private val ratingRepo: RatingRepo
) : ViewModel() {
    private val _userDetails = MutableStateFlow(LoginResponse.Data.UserDetail())
    val userDetails = _userDetails.asStateFlow()

    private val _ordersState = MutableStateFlow<NetworkResult<WorkResponse>>(NetworkResult.Idle())
    val ordersState = _ordersState.asStateFlow()

    private val _acceptOrdersState =
        MutableStateFlow<NetworkResult<UpdateWorkStateResponse>>(NetworkResult.Idle())
    val acceptOrdersState = _acceptOrdersState.asStateFlow()

    private val _rejectOrdersState =
        MutableStateFlow<NetworkResult<UpdateWorkStateResponse>>(NetworkResult.Idle())
    val rejectOrdersState = _rejectOrdersState.asStateFlow()

    private val _markCompleteOrdersState =
        MutableStateFlow<NetworkResult<MarkCompleteWorkResponse>>(NetworkResult.Idle())
    val markCompleteOrdersState = _markCompleteOrdersState.asStateFlow()

    private val _sendFeedbackState =
        MutableStateFlow<NetworkResult<UploadRatingResponse>>(NetworkResult.Idle())
    val sendFeedbackState = _sendFeedbackState.asStateFlow()

    init {
        viewModelScope.launch {
            appDataStore.getUserFromDataStore().collectLatest {
                _userDetails.value = it
            }
        }
    }

    fun getOrdersForCustomer(state: OrderState, customerId: Int) {
        viewModelScope.launch {
            when (state) {
                OrderState.RUNNING -> {
                    workRepo.getCustomerRunningRequests(customerId).collectLatest {
                        _ordersState.value = it
                    }
                }
                OrderState.REQUESTED -> {
                    workRepo.getCustomerPendingRequests(customerId).collectLatest {
                        _ordersState.value = it
                    }
                }
                OrderState.COMPLETED -> {
                    workRepo.getCustomerCompletedRequests(customerId).collectLatest {
                        _ordersState.value = it
                    }
                }
                OrderState.REJECTED -> {
                    workRepo.getCustomerRejectedRequests(customerId).collectLatest {
                        _ordersState.value = it
                    }
                }
            }
        }

    }

    fun getOrdersForWorker(state: OrderState, workerId: Int) {
        viewModelScope.launch {
            when (state) {
                OrderState.RUNNING -> {
                    workRepo.getWorkerRunningRequests(workerId).collectLatest {
                        _ordersState.value = it
                    }
                }
                OrderState.REQUESTED -> {
                    workRepo.getWorkerReceivedRequests(workerId).collectLatest {
                        _ordersState.value = it
                    }
                }
                OrderState.COMPLETED -> {
                    workRepo.getWorkerCompletedRequests(workerId).collectLatest {
                        _ordersState.value = it
                    }
                }
                OrderState.REJECTED -> {
                    workRepo.getWorkerPendingRequests(workerId).collectLatest {
                        _ordersState.value = it
                    }
                }
            }
        }

    }

    fun acceptOffer(workerId: Int, workId: Int) {
        viewModelScope.launch {
            workRepo.workAcceptByWorkerRequest(workerId, workId).collectLatest {
                _acceptOrdersState.value = it
            }
        }
    }

    fun rejectOffer(workerId: Int, workId: Int) {
        viewModelScope.launch {
            workRepo.workRejectByWorkerRequest(workerId, workId).collectLatest {
                _rejectOrdersState.value = it
            }
        }
    }

    fun onOrderMarkAsComplete(customerId: Int, workId: Int) {
        viewModelScope.launch {
            workRepo.customerMarkRequestCompleted(customerId, workId)
                .collectLatest {
                    _markCompleteOrdersState.value = it
                }
        }
    }

    fun sendFeedbackToWorker(request: RatingRequest) {
        viewModelScope.launch {
            ratingRepo.postRating(request)
                .collectLatest {
                    _sendFeedbackState.value = it
                }
        }
    }

}