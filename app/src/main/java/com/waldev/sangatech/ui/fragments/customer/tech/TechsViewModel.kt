package com.waldev.sangatech.ui.fragments.customer.tech

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.repos.categories.CategoriesRepo
import com.waldev.sangatech.responses.categories.CategoryUsersResponse
import com.waldev.sangatech.sealed.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TechsViewModel @Inject constructor(
    private val categoriesRepo: CategoriesRepo
) : ViewModel() {
    var catId: Int = 0
    var catName: String = ""

    private val _catUsersState =
        MutableStateFlow<NetworkResult<CategoryUsersResponse>>(NetworkResult.Idle())
    val catUsersState = _catUsersState.asStateFlow()

    fun fetchUsersByCategory() {
        viewModelScope.launch {
            categoriesRepo.getAllUsersByCategory(catId).collectLatest {
                _catUsersState.value = it
            }
        }
    }
}