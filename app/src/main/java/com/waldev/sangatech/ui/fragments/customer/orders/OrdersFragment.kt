package com.waldev.sangatech.ui.fragments.customer.orders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.tabs.TabLayoutMediator
import com.waldev.sangatech.databinding.OrdersFragmentBinding
import com.waldev.sangatech.ui.fragments.customer.orders.adapters.OrdersPagerAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OrdersFragment : Fragment() {
    private var _binding: OrdersFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: OrdersViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = OrdersFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPager()
    }

    private fun initPager() {
        val pagerAdapter = OrdersPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)
        binding.pagerOrders.adapter = pagerAdapter
        TabLayoutMediator(binding.tablayout, binding.pagerOrders) { tab, position ->
            tab.text = getTitle(position)
        }.attach()
    }

    private fun getTitle(position: Int): String {
        return if (position == 0) {
            "Running"
        } else if (position == 1) {
            "Order Request"
        } else if (position == 2) {
            "Completed"
        } else if (position == 3) {
            "Rejected"
        } else {
            "invalid"
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}