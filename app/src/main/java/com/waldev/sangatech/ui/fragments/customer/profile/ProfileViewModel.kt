package com.waldev.sangatech.ui.fragments.customer.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.repos.rating.RatingRepo
import com.waldev.sangatech.repos.users.UsersRepo
import com.waldev.sangatech.responses.rating.WorkerRatingsResponse
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.utils.AppDataStore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val usersRepo: UsersRepo,
    private val appDataStore: AppDataStore,
    private val ratingRepo: RatingRepo
) : ViewModel() {

    private val _updateUserState =
        MutableStateFlow<NetworkResult<LoginResponse>>(NetworkResult.Idle())
    val updateUserState = _updateUserState.asStateFlow()
    private val _userDetails = MutableStateFlow(LoginResponse.Data.UserDetail())
    val userDetails = _userDetails.asStateFlow()

    private val _workerFeedbacksState =
        MutableStateFlow<NetworkResult<WorkerRatingsResponse>>(NetworkResult.Idle())
    val workerFeedbacksState = _workerFeedbacksState.asStateFlow()

    init {
        viewModelScope.launch {
            appDataStore.getUserFromDataStore().collectLatest {
                _userDetails.value = it
            }
        }
    }


    fun updateUser(
        id: Int,
        name: String? = null,
        email: String,
        password: String? = null,
        category: Int? = null,
        address1: String? = null,
        address2: String? = null,
        latitude: Double? = null,
        longitude: Double? = null,
        profile_pic: File? = null,
        desc: String? = null,
    ) {
        viewModelScope.launch {
            usersRepo.updateUserProfile(
                id,
                name,
                email,
                password,
                category,
                address1,
                address2,
                latitude,
                longitude,
                profile_pic,
                desc
            )
                .collectLatest {
                    _updateUserState.value = it
                }
        }
    }


    fun saveUpdatedUser(details: LoginResponse.Data.UserDetail) {
        viewModelScope.launch {
            appDataStore.saveUserModel(details)
        }
    }

    fun getWorkerFeedbacks(workerId: Int) {
        viewModelScope.launch {
            ratingRepo.getRatingsByWorkerId(workerId).collectLatest {
                _workerFeedbacksState.value = it
            }
        }
    }

    fun logout() {
        viewModelScope.launch {
            appDataStore.saveUserModel(LoginResponse.Data.UserDetail())
        }

    }

}