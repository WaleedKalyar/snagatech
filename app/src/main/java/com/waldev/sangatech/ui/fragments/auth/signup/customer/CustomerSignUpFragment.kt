package com.waldev.sangatech.ui.fragments.auth.signup.customer

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.waldev.sangatech.databinding.FragmentCustomerSignUpBinding
import com.waldev.sangatech.enums.ResponseStatus
import com.waldev.sangatech.requests.SignUpRequest
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.activities.main.customer.CustomerMainActivity
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.ui.fragments.auth.signup.container.SignUpContainerViewModel
import com.waldev.sangatech.utils.AppDataStore
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import com.waldev.sangatech.utils.ExtensionUtils.hideWaitingDialogIfVisible
import com.waldev.sangatech.utils.ExtensionUtils.showOrUpdateWaitingDialog
import com.waldev.sangatech.utils.ToastUtility
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class CustomerSignUpFragment(
    private val onBackPress: () -> Unit
) : Fragment() {
    private var _binding: FragmentCustomerSignUpBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SignUpContainerViewModel by activityViewModels()

    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }

    @Inject
    lateinit var appDataStore: AppDataStore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentCustomerSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initCollectors()
        initClickListeners()
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.singUpState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    ToastUtility.errorToast(requireContext(), it.message)
                    Timber.d("Error -> ${it.message}")
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Signing Up")
                }
                is NetworkResult.Success -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    it.result?.let { resp ->
                        if (resp.status == ResponseStatus.success) {
                            Timber.d("SignUP: Customer response -> ${resp.data.userDetail.toString()}")
                            val userInfo = LoginResponse.Data.UserDetail(
                                address_1 = resp.data.userDetail.address_1,
                                address_2 = resp.data.userDetail.address_2,
                                category = 0,
                                created_at = resp.data.userDetail.created_at,
                                desc = "",
                                email = resp.data.userDetail.email,
                                email_verified_at = "",
                                id = resp.data.userDetail.id,
                                latitude = resp.data.userDetail.latitude,
                                longitude = resp.data.userDetail.longitude,
                                name = resp.data.userDetail.name,
                                profile_img = "",
                                profile_url = "",
                                role = resp.data.userDetail.role,
                                status = 0,
                                type = resp.data.userDetail.type,
                                updated_at = resp.data.userDetail.updated_at
                            )
                            Timber.d("SignUp Customer Info -> $userInfo")
                            appDataStore.saveUserModel(userInfo)
                            resp.data.userDetail
                            navigateToMainScreen()
                        } else {
                            ToastUtility.errorToast(
                                requireContext(),
                                "Something went wrong! error-> ${resp.message}"
                            )
                            Timber.d("SingUp: Customer resp -> ${resp.toString()}")
                        }

                    }
                }
            }
        }
    }


    private fun initClickListeners() {
        binding.btnSignUp.setOnClickListener {
            validateInputs()
        }
        binding.tvLogin.setOnClickListener {
            onBackPress()
        }
    }

    private fun validateInputs() {
        binding.apply {
            val email = etEmail.text.toString()
            val name = etName.text.toString()
            val address1 = etAddress1.text.toString()
            val address2 = etAddress2.text.toString()
            val password = etPassword.text.toString()
            val confirmPass = etConfirmPass.text.toString()
            if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                inputEmail.error = "Email is empty or invalid"
            } else if (name.isEmpty()) {
                inputEmail.error = null
                inputName.error = "Please enter your name"
            } else if (address1.isEmpty()) {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = "Please fill your primary address"
            } else if (address2.isEmpty()) {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = null
                inputAddress2.error = "Please fill your secondary address"
            } else if (password.isEmpty() || password.length < 6) {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = null
                inputAddress2.error = null
                inputPassword.error = "Password must be at least 6 characters"
            } else if (confirmPass != password) {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = null
                inputAddress2.error = null
                inputPassword.error = null
                inputConfirmPass.error = "Password does not match"
            } else {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = null
                inputAddress2.error = null
                inputPassword.error = null
                inputConfirmPass.error = null

                viewModel.registerNewUser(
                    SignUpRequest(
                        name = name,
                        email = email,
                        password = password,
                        category = 0, // don't have category because its customer/ 0 means customer
                        address1 = address1,
                        address2 = address2,
                        latitude = viewModel.location.latitude,
                        longitude = viewModel.location.longitude
                    )
                )
            }
        }
    }

    private fun navigateToMainScreen() {
        Intent(requireContext(), CustomerMainActivity::class.java).apply {
            startActivity(this)
            requireActivity().finishAffinity()
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}