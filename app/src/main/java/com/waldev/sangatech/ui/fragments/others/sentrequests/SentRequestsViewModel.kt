package com.waldev.sangatech.ui.fragments.others.sentrequests

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SentRequestsViewModel @Inject constructor() : ViewModel() {

}