package com.waldev.sangatech.ui.fragments.worker.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.waldev.sangatech.databinding.SellerHomeFragmentBinding
import com.waldev.sangatech.enums.UserType
import com.waldev.sangatech.responses.user.UserDetailsResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.ui.fragments.worker.home.adapters.MonthsAdapter
import com.waldev.sangatech.ui.fragments.worker.home.adapters.RecentOrdersAdapter
import com.waldev.sangatech.utils.AppConsts
import com.waldev.sangatech.utils.AppConsts.getGeneric12Months
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import com.waldev.sangatech.utils.ExtensionUtils.hideWaitingDialogIfVisible
import com.waldev.sangatech.utils.ExtensionUtils.showOrUpdateWaitingDialog
import com.waldev.sangatech.utils.ToastUtility
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class SellerHomeFragment : Fragment() {
    private var _binding: SellerHomeFragmentBinding? = null
    private val binding get() = _binding!!


    private val viewModel: SellerHomeViewModel by viewModels()

    private val adapter: MonthsAdapter by lazy { MonthsAdapter() }

    private lateinit var resentsAdapter: RecentOrdersAdapter

    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SellerHomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMonthsRecycler()
        initResentsRecycler()
        initCollectors()
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.userDetails) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    Timber.d("WorkerDetails -> Error ${it.message}")
                    ToastUtility.errorToast(
                        requireContext(),
                        "Something went wrong! please try again"
                    )
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Fetching Info")
                }
                is NetworkResult.Success -> {
                    Timber.d("WorkerDetails : Response -> ${it.result?.status}")
                    hideWaitingDialogIfVisible(waitingDialog)
                    if (it.result != null) {
                        setWorkerDetailsInfo(it.result.data.catUsers)
                    } else {
                        ToastUtility.warningToast(
                            requireContext(),
                            "User Details data not available"
                        )
                    }
                }
            }
        }
        collectLatestLifecycleFlow(viewModel.recentWorks) {
            when (it) {
                is NetworkResult.Error -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.VISIBLE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerOrders.visibility = View.GONE
                    }
                }
                is NetworkResult.Idle -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerOrders.visibility = View.GONE
                    }
                }
                is NetworkResult.Loading -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.startShimmer()
                        shimmerLayout.visibility = View.VISIBLE
                        recyclerOrders.visibility = View.GONE
                    }
                }
                is NetworkResult.Success -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerOrders.visibility = View.GONE
                    }
                    Timber.d("Recent Orders: success resp -> ${it.result.toString()}")
                    if (it.result != null && it.result.data.workReq.isNotEmpty()) {
                        binding.recyclerOrders.visibility = View.VISIBLE
                        resentsAdapter.submitList(it.result.data.workReq)
                    } else {
                        binding.rlNoData.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun setWorkerDetailsInfo(details: UserDetailsResponse.Data.CatUsers) {
        val list = getGeneric12Months()
        for (i in 0 until list.size) {
            list[i].earnings = details.earnings[i]
        }
        adapter.submitList(list)

        binding.apply {
            tvTotalEarnings.text = String.format("$%.1f", details.total_earning)
        }
    }

    private fun initResentsRecycler() {
        var userType = UserType.CUSTOMER
        if (viewModel.userInfo.value.type == AppConsts.USER_TYPE_WORKER) {
            userType = UserType.WORKER
        }
        resentsAdapter = RecentOrdersAdapter(userType)
        binding.recyclerOrders.adapter = resentsAdapter
    }

    private fun initMonthsRecycler() {
        binding.recyclerMonths.adapter = adapter
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}