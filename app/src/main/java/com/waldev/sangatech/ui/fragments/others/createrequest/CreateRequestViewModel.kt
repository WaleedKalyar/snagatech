package com.waldev.sangatech.ui.fragments.others.createrequest

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.repos.work.WorkRepo
import com.waldev.sangatech.requests.PostWorkRequest
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.responses.work.PostWorkResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.utils.AppDataStore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class CreateRequestViewModel @Inject constructor(
    private val appDataStore: AppDataStore,
    private val workRepo: WorkRepo
) : ViewModel() {

    var workerName: String = ""
    var categoryId: Int = 0
    var workerId: Int = 0
    var calendar: Calendar = Calendar.getInstance()

    private val _userDetails = MutableStateFlow(LoginResponse.Data.UserDetail())
    val userDetails = _userDetails.asStateFlow()

    private val _uploadWorkState =
        MutableStateFlow<NetworkResult<PostWorkResponse>>(NetworkResult.Idle())
    val uploadWorkState = _uploadWorkState.asStateFlow()


    init {
        viewModelScope.launch {
            appDataStore.getUserFromDataStore().collectLatest {
                _userDetails.value = it
            }
        }
    }


    fun postNewWorkRequest(request: PostWorkRequest) {
        viewModelScope.launch {
            workRepo.addNewWorkRequest(request).collectLatest {
                _uploadWorkState.value = it
            }
        }
    }

}