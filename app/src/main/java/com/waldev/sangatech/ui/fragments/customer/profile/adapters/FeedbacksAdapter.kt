package com.waldev.sangatech.ui.fragments.customer.profile.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RatingBar
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.ItemFeedbackBinding
import com.waldev.sangatech.responses.rating.WorkerRatingsResponse
import timber.log.Timber

class FeedbacksAdapter :
    ListAdapter<WorkerRatingsResponse.Data.Rating, FeedbacksAdapter.FeedbackViewHolder>(DiffCallback) {

    inner class FeedbackViewHolder(private val binding: ItemFeedbackBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: WorkerRatingsResponse.Data.Rating) {
            binding.apply {
                if (model.customer_detail.profile_url != null && model.customer_detail.profile_url!!.isNotEmpty()) {
                    imageProfile.load(model.customer_detail.profile_url)
                } else {
                    imageProfile.setImageResource(R.drawable.ic_profile)
                }
                tvName.text = model.customer_detail.name
                tvRating.text = String.format("%.1f", model.rate)
                tvComment.text = model.review
                Timber.d("Rating: is -> ${model.rate}")
               feedbackRating?.rating = model.rate.toFloat()
                Timber.d("Rating: get -> ${feedbackRating?.rating}")
            }
        }
    }

    object DiffCallback : DiffUtil.ItemCallback<WorkerRatingsResponse.Data.Rating>() {

        override fun areItemsTheSame(
            oldItem: WorkerRatingsResponse.Data.Rating,
            newItem: WorkerRatingsResponse.Data.Rating
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: WorkerRatingsResponse.Data.Rating,
            newItem: WorkerRatingsResponse.Data.Rating
        ): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackViewHolder {
        return FeedbackViewHolder(
            ItemFeedbackBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: FeedbackViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}