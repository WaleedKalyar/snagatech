package com.waldev.sangatech.ui.fragments.customer.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.HomeFragmentBinding
import com.waldev.sangatech.enums.ResponseStatus
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.dialogs.drawer.NavDrawerDialog
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.ui.fragments.customer.home.adapters.CategoriesAdapter
import com.waldev.sangatech.utils.AppConsts.CATEGORY_ID
import com.waldev.sangatech.utils.AppConsts.CATEGORY_NAME
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private var _binding: HomeFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: HomeViewModel by viewModels()
    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }

    private lateinit var adapter: CategoriesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initAdapter()
        initClickListeners()
        initCollectors()
    }

    @SuppressLint("SetTextI18n")
    private fun initViews() {
        binding.tvUserName.text = "hi ${viewModel.currentUserState.value.name}"
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.catsUiState) {
            when (it) {
                is NetworkResult.Error -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.VISIBLE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerCats.visibility = View.GONE
                    }
                }
                is NetworkResult.Idle -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerCats.visibility = View.GONE
                    }
                }
                is NetworkResult.Loading -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.startShimmer()
                        shimmerLayout.visibility = View.VISIBLE
                        recyclerCats.visibility = View.GONE
                    }
                }
                is NetworkResult.Success -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerCats.visibility = View.GONE
                    }
                    it.result?.let { result ->
                        if (result.status == ResponseStatus.success && result.data.catDetail.isNotEmpty()) {
                            binding.recyclerCats.visibility = View.VISIBLE
                            adapter.submitList(result.data.catDetail)
                        } else {
                            binding.rlNoData.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }
    }

    private fun initAdapter() {
        adapter = CategoriesAdapter {
            val bundle = Bundle()
            bundle.putInt(CATEGORY_ID, it.id)
            bundle.putString(CATEGORY_NAME, it.name)
            findNavController().navigate(R.id.action_homeFragment_to_techsFragment, bundle)
        }
        binding.recyclerCats.adapter = adapter
    }

    private fun initClickListeners() {
        binding.btnNav.setOnClickListener {
            NavDrawerDialog(requireContext(),
                onCreateRequestClick = {
                    findNavController().navigate(R.id.action_homeFragment_to_createRequestFragment)
                },
                onViewSentRequestsClick = {
                    findNavController().navigate(R.id.action_homeFragment_to_sentRequestsFragment)
                }
            ).show()
        }
        binding.tvTechMap.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_techsMapFragment)
        }
        binding.btnRetry.setOnClickListener {
            viewModel.fetchCategories()
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}