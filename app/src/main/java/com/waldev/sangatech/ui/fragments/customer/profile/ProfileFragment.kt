package com.waldev.sangatech.ui.fragments.customer.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import coil.load
import com.canhub.cropper.CropImageContract
import com.canhub.cropper.CropImageContractOptions
import com.canhub.cropper.CropImageView
import com.canhub.cropper.options
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.ProfileFragmentBinding
import com.waldev.sangatech.enums.ResponseStatus
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.activities.auth.AuthActivity
import com.waldev.sangatech.ui.dialogs.update.UpdateInfoDialog
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.ui.fragments.customer.profile.adapters.FeedbacksAdapter
import com.waldev.sangatech.utils.AppConsts.USER_TYPE_WORKER
import com.waldev.sangatech.utils.CenterZoomLayoutManager
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import com.waldev.sangatech.utils.ExtensionUtils.hideWaitingDialogIfVisible
import com.waldev.sangatech.utils.ExtensionUtils.showOrUpdateWaitingDialog
import com.waldev.sangatech.utils.ToastUtility
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.io.File

@AndroidEntryPoint
class ProfileFragment : Fragment() {
    private var _binding: ProfileFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ProfileViewModel by viewModels()
    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }

    private lateinit var imageLauncher: ActivityResultLauncher<CropImageContractOptions>

    private val adapter: FeedbacksAdapter by lazy { FeedbacksAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ProfileFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLaunchers()
        initCollectors()
        initViews()
        initClickListeners()
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.updateUserState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    ToastUtility.errorToast(requireContext(), it.message)
                    Timber.d("Error -> ${it.message}")
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Updating Info")
                }
                is NetworkResult.Success -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    it.result?.let { result ->
                        Timber.d("Success : resp -> ${result.toString()}")
                        if (result.status == ResponseStatus.success) {
                            viewModel.saveUpdatedUser(result.data.userDetail)
                            ToastUtility.successToast(
                                requireContext(),
                                "Profile Updated successfully"
                            )
                        } else {
                            ToastUtility.warningToast(
                                requireContext(),
                                "Something went wrong!"
                            )
                        }
                    }


                }
            }
        }
        collectLatestLifecycleFlow(viewModel.workerFeedbacksState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    ToastUtility.errorToast(requireContext(), it.message)
                    Timber.d("Error -> ${it.message}")
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Fetching Feedbacks")
                }
                is NetworkResult.Success -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    it.result?.let { result ->
                        Timber.d("Success : resp -> ${result.toString()}")
                        if (result.status == ResponseStatus.success) {
                            adapter.submitList(result.data.ratings)
                        } else {
                            ToastUtility.warningToast(
                                requireContext(),
                                "Something went wrong!"
                            )
                        }
                    }


                }
            }
        }
    }

    private fun initClickListeners() {
        binding.btnPickProfile.setOnClickListener {
            imageLauncher.launch(options {
                setFixAspectRatio(true)
                setRequestedSize(
                    480, 480,
                    CropImageView.RequestSizeOptions.RESIZE_FIT
                )
                setCropShape(CropImageView.CropShape.OVAL)
                setAspectRatio(1, 1)
            })
        }

        binding.btnEditDescription.setOnClickListener {
            UpdateInfoDialog(
                context = requireContext(),
                title = "Description",
                lines = 5,
                hint = "Enter about yourself",
                oldInfo = viewModel.userDetails.value.desc.toString()
            ) {
                viewModel.updateUser(
                    id = viewModel.userDetails.value.id,
                    email = viewModel.userDetails.value.email,
                    desc = it,
                )
            }.show()
        }

        binding.btnEditAddress.setOnClickListener {
            UpdateInfoDialog(
                context = requireContext(),
                title = "Address",
                lines = 2,
                hint = "Enter about Address",
                oldInfo = viewModel.userDetails.value.address_1.toString()
            ) {
                viewModel.updateUser(
                    id = viewModel.userDetails.value.id,
                    email = viewModel.userDetails.value.email,
                    address1 = it,
                    address2 = it,
                )
            }.show()
        }

        binding.btnLogout.setOnClickListener {
            viewModel.logout()
            Intent(requireContext(), AuthActivity::class.java).apply {
                startActivity(this)
                requireActivity().finishAffinity()
            }
        }
    }

    private fun initViews() {
        collectLatestLifecycleFlow(viewModel.userDetails) { userDetail ->
            binding.apply {
                Timber.d("ProfilePage: UserId -> ${userDetail.id} Profile Url -> ${userDetail.profile_url} Profile Image -> ${userDetail.profile_img}")
                if (userDetail.profile_url != null && userDetail.profile_url!!.isNotEmpty()) {
                    imageProfile.load(userDetail.profile_url)
                } else {
                    imageProfile.setImageResource(R.drawable.ic_profile)
                }
                tvName.text = userDetail.name
                tvDescription.text = userDetail.desc
                tvAddress.text = userDetail.address_1

                if (userDetail.type == USER_TYPE_WORKER) {
                    tvFeedbacks.visibility = View.VISIBLE
                    feedbackRecycler.visibility = View.VISIBLE
                    tvRating.visibility = View.VISIBLE
                    initFeedbacksInfo(userDetail.id)
                } else {
                    tvFeedbacks.visibility = View.GONE
                    feedbackRecycler.visibility = View.GONE
                    tvRating.visibility = View.GONE
                }
            }
        }
    }

    private fun initFeedbacksInfo(workerId: Int) {
        val layoutManager = CenterZoomLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.feedbackRecycler.layoutManager = layoutManager
        val pagerSnapHelper = PagerSnapHelper()
        binding.feedbackRecycler.onFlingListener = null
        pagerSnapHelper.attachToRecyclerView(binding.feedbackRecycler)
        binding.feedbackRecycler.adapter = adapter

        viewModel.getWorkerFeedbacks(workerId)
    }

    private fun initLaunchers() {
        imageLauncher = this.registerForActivityResult(CropImageContract()) { result ->
            if (result.isSuccessful) {
                val uriContent = result.uriContent
                Timber.d("Content Uri -> $uriContent")
                binding.imageProfile.setImageURI(uriContent)
                val filePathUri = result.getUriFilePath(requireContext(), true)!!
                Timber.d("File Path -> $filePathUri")
                val file = File(filePathUri)
                if (file.exists()) {
                    Timber.d("File created and exist")
                    Timber.d("Existing Email Address -> ${viewModel.userDetails.value.email}")
                    viewModel.updateUser(
                        id = viewModel.userDetails.value.id,
                        email = viewModel.userDetails.value.email,
                        profile_pic = File(filePathUri)
                    )
                } else {
                    Timber.d("File not exist")
                }

            } else {
                ToastUtility.errorToast(
                    requireContext(),
                    "Something went wrong!${result.error?.message}"
                )
            }
        }
    }


    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }


}