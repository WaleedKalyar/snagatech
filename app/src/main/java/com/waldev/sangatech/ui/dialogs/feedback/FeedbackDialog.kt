package com.waldev.sangatech.ui.dialogs.feedback

import android.R
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import com.waldev.sangatech.databinding.DialogFeedbackBinding
import com.waldev.sangatech.requests.RatingRequest
import com.waldev.sangatech.responses.work.MarkCompleteWorkResponse
import com.waldev.sangatech.responses.work.UpdateWorkStateResponse
import java.util.*

class FeedbackDialog(
    context: Context,
    private val order: MarkCompleteWorkResponse.Data.WorkReq,
    private val onReviewSubmit: (RatingRequest) -> Unit
) : Dialog(context) {
    private var _binding: DialogFeedbackBinding? = null
    private val binding get() = _binding!!

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        _binding = DialogFeedbackBinding.inflate(LayoutInflater.from(context))
        setContentView(binding.root)
        Objects.requireNonNull(window)?.setBackgroundDrawable(
            ColorDrawable(
                context.resources.getColor(
                    R.color.white,
                    null,
                )
            )
        )
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        init()
    }

    private fun init() {
        initClickListeners()
    }

    private fun initClickListeners() {
        binding.btnBack.setOnClickListener {
            dismiss()
        }

        binding.btnSkip.setOnClickListener {
            dismiss()
        }

        binding.btnConfirm.setOnClickListener {
            val rating = binding.ratingbar.rating
            val review = binding.etReview.text.toString()
            val request = RatingRequest(
                work_id = order.id,
                cus_id = order.cus_id,
                worker_id = order.worker_id,
                rate = rating,
                review = review
            )

            onReviewSubmit(request)
            dismiss()

        }
    }
}