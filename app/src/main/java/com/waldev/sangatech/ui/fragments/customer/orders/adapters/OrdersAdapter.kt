package com.waldev.sangatech.ui.fragments.customer.orders.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.ItemOrderBinding
import com.waldev.sangatech.enums.OrderState
import com.waldev.sangatech.enums.UserType
import com.waldev.sangatech.responses.work.WorkResponse
import com.waldev.sangatech.utils.DateUtilities
import java.util.*

class OrdersAdapter(
    private val orderState: OrderState,
    private val userType: UserType,
    private val onOrderClick: (WorkResponse.Data.WorkReq, UserType) -> Unit
) :
    ListAdapter<WorkResponse.Data.WorkReq, OrdersAdapter.OrderViewHolder>(DiffCallback) {

    inner class OrderViewHolder(private val binding: ItemOrderBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(model: WorkResponse.Data.WorkReq, orderState: OrderState, userType: UserType) {
            binding.apply {
                tvTitle.text = model.title
                tvAmount.text = String.format("$%.2f", model.amount)
                if (userType == UserType.WORKER) {
                    binding.titleOrderBy.text = "Order By"
                    tvUserName.text = model.customer_detail.name
                } else {
                    binding.titleOrderBy.text = "Order To"
                    tvUserName.text = model.worker_detail.name
                }

                tvId.text = "S2OI-${Calendar.getInstance().get(Calendar.YEAR)}-${model.id}"

                tvDate.text = DateUtilities.getDateFromMillis(model.arr_time.toLong())
                tvTime.text = DateUtilities.getTimeFromMillis(model.arr_time.toLong())

                when (orderState) {
                    OrderState.RUNNING -> {
                        chipStatus.setChipBackgroundColorResource(R.color.colorBlue)
                        chipStatus.text = "Running"
                        rlTop.setBackgroundColor(ContextCompat.getColor(root.context,R.color.colorBlue))
                    }
                    OrderState.REQUESTED -> {
                        chipStatus.setChipBackgroundColorResource(R.color.colorYellow)
                        rlTop.setBackgroundColor(ContextCompat.getColor(root.context,R.color.colorYellow))
//                        chipStatus.setBackgroundColor(
//                            ContextCompat.getColor(
//                                this.root.context,
//                                R.color.colorYellow
//                            )
//                        )
                        if (userType == UserType.CUSTOMER) {
                            chipStatus.text = "Sent"
                        } else {
                            chipStatus.text = "Pending"
                        }

                    }
                    OrderState.COMPLETED -> {
                        chipStatus.setChipBackgroundColorResource(R.color.colorGreen)
                        rlTop.setBackgroundColor(ContextCompat.getColor(root.context,R.color.colorGreen))
//                        chipStatus.setBackgroundColor(
//                            ContextCompat.getColor(
//                                this.root.context,
//                                R.color.colorGreen
//                            )
//                        )
                        chipStatus.text = "Completed"
                    }
                    OrderState.REJECTED -> {
                        chipStatus.setChipBackgroundColorResource(R.color.colorRed)
                        rlTop.setBackgroundColor(ContextCompat.getColor(root.context,R.color.colorRed))
//                        chipStatus.setBackgroundColor(
//                            ContextCompat.getColor(
//                                this.root.context,
//                                R.color.colorRed
//                            )
//                        )
                        chipStatus.text = "Rejected"
                    }
                }

                binding.root.setOnClickListener {
                    onOrderClick(model, userType)
                }
            }
        }

    }


    object DiffCallback : DiffUtil.ItemCallback<WorkResponse.Data.WorkReq>() {

        override fun areItemsTheSame(
            oldItem: WorkResponse.Data.WorkReq,
            newItem: WorkResponse.Data.WorkReq
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: WorkResponse.Data.WorkReq,
            newItem: WorkResponse.Data.WorkReq
        ): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        return OrderViewHolder(
            ItemOrderBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.bind(getItem(position), orderState, userType)
    }
}