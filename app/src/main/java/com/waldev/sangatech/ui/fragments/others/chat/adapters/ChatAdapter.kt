package com.waldev.sangatech.ui.fragments.others.chat.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.waldev.sangatech.databinding.MessageItemLeftBinding
import com.waldev.sangatech.databinding.MessageItemRightBinding
import com.waldev.sangatech.enums.MessageType
import com.waldev.sangatech.responses.chat.ConversationMessagesResponse
import com.waldev.sangatech.utils.DateUtilities
import timber.log.Timber

class ChatAdapter(
    private val currentUserId: Int,
    private val onMessageAction: (ConversationMessagesResponse.Data.Conversation, View) -> Unit,
    //private val onLocationClick: (LocationModel, String) -> Unit,
) :
    ListAdapter<ConversationMessagesResponse.Data.Conversation, RecyclerView.ViewHolder>(
        ChatDiffCallback
    ) {


    inner class LeftItemViewHolder(val binding: MessageItemLeftBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: ConversationMessagesResponse.Data.Conversation) {
            binding.timeText.text =
                DateUtilities.getTimeAgo(DateUtilities.getMillisFromStringDate(model.created_at))
//            if (model.deleted) {
//                binding.messageText.visibility = View.VISIBLE
//                binding.messageImage.visibility = View.GONE
//                binding.messageAudio.visibility = View.GONE
//                binding.messageVideo.visibility = View.GONE
//
//                binding.messageText.text = DELETED_MESSAGE
//                binding.messageText.setTypeface(null, Typeface.BOLD_ITALIC)
//            } else {
            when (model.type) {
                MessageType.text -> {
                    binding.messageText.visibility = View.VISIBLE
                    binding.messageImage.visibility = View.GONE
                    binding.messageAudio.visibility = View.GONE
                    binding.messageVideo.visibility = View.GONE
                    binding.messageText.text = model.msg
                }
                MessageType.image -> {
                    binding.messageText.visibility = View.GONE
                    binding.messageImage.visibility = View.VISIBLE
                    binding.messageAudio.visibility = View.GONE
                    binding.messageVideo.visibility = View.GONE
                    Glide.with(binding.root.context).load(model.file_url)
                        .into(binding.messageImage)
                }
                else -> {

                }
            }
            // }

        }
    }

    inner class RightItemViewHolder(val binding: MessageItemRightBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: ConversationMessagesResponse.Data.Conversation) {
            binding.timeText.text =
                DateUtilities.getTimeAgo(DateUtilities.getMillisFromStringDate(model.created_at))

            binding.messageText.setOnLongClickListener {
                onMessageAction(model, it)
                false
            }
            binding.messageImage.setOnLongClickListener {
                onMessageAction(model, it)
                false
            }
//            if (model.deleted) {
//                binding.messageText.visibility = View.VISIBLE
//                binding.messageImage.visibility = View.GONE
//                binding.messageAudio.visibility = View.GONE
//                binding.messageVideo.visibility = View.GONE
//
//                binding.messageText.text = DELETED_MESSAGE
//                binding.messageText.setTypeface(null, Typeface.BOLD_ITALIC)
//            } else {
            when (model.type) {
                MessageType.text -> {
                    binding.messageText.visibility = View.VISIBLE
                    binding.messageImage.visibility = View.GONE
                    binding.messageAudio.visibility = View.GONE
                    binding.messageVideo.visibility = View.GONE

                    binding.messageText.text = model.msg
                }
                MessageType.image -> {
                    binding.messageText.visibility = View.GONE
                    binding.messageImage.visibility = View.VISIBLE
                    binding.messageAudio.visibility = View.GONE
                    binding.messageVideo.visibility = View.GONE
                    Glide.with(binding.root.context).load(model.file_url)
                        .into(binding.messageImage)
                }
                else -> {

                }
            }
            //}

        }
    }


    object ChatDiffCallback :
        DiffUtil.ItemCallback<ConversationMessagesResponse.Data.Conversation>() {

        override fun areItemsTheSame(
            oldItem: ConversationMessagesResponse.Data.Conversation,
            newItem: ConversationMessagesResponse.Data.Conversation
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: ConversationMessagesResponse.Data.Conversation,
            newItem: ConversationMessagesResponse.Data.Conversation
        ): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == LEFT_CHAT_ITEM) {
            return LeftItemViewHolder(
                MessageItemLeftBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            return RightItemViewHolder(
                MessageItemRightBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LeftItemViewHolder) {
            holder.bind(getItem(position))
        } else if (holder is RightItemViewHolder) {
            holder.bind(getItem(position))
        }
    }

    override fun getItemViewType(position: Int): Int {
        // Case 0 -> Left item
        // Case 1 -> Right item
        val model = getItem(position)
        Timber.d("ChatAdapter: current userId -> $currentUserId")
        return if (model.get_sender_detail.id == currentUserId) {
            Timber.d("ChatAdapter: ID contains must be right")
            RIGHT_CHAT_ITEM
        } else {
            Timber.d("ChatAdapter: ID not contains must be left")
            LEFT_CHAT_ITEM
        }

    }

    companion object {
        const val LEFT_CHAT_ITEM = 0
        const val RIGHT_CHAT_ITEM = 1
    }
}