package com.waldev.sangatech.ui.fragments.customer.tech.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.ItemTechBinding
import com.waldev.sangatech.responses.categories.CategoryUsersResponse
import timber.log.Timber

class TechsAdapter(
    private val catName: String,
    private val onChatClick: (CategoryUsersResponse.Data.CatUser) -> Unit,
    private val onSendOfferClick: (CategoryUsersResponse.Data.CatUser) -> Unit,
    private val onUserClick: (Int) -> Unit,
) : ListAdapter<CategoryUsersResponse.Data.CatUser, TechsAdapter.TechViewHolder>(DiffCallback) {

    inner class TechViewHolder(private val binding: ItemTechBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(model: CategoryUsersResponse.Data.CatUser) {
            binding.apply {
                tvName.text = model.name
                Timber.d("TechAdapter: Profile url -> ${model.profile_url}")
                if (model.profile_url != null) {
                    imageProfile.load(model.profile_url)
                } else {
                    imageProfile.setImageResource(R.drawable.ic_profile)
                }
                tvExpertIn.text = "${catName.uppercase()} Expert"

                tvDescription.text = model.desc


                btnChat.setOnClickListener {
                    onChatClick(model)
                }
                btnSendOffer.setOnClickListener {
                    onSendOfferClick(model)
                }

                binding.imageProfile.setOnClickListener {
                    onUserClick(model.id)
                }

                binding.root.setOnClickListener {
                    onUserClick(model.id)
                }

            }
        }
    }


    object DiffCallback : DiffUtil.ItemCallback<CategoryUsersResponse.Data.CatUser>() {

        override fun areItemsTheSame(
            oldItem: CategoryUsersResponse.Data.CatUser,
            newItem: CategoryUsersResponse.Data.CatUser
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: CategoryUsersResponse.Data.CatUser,
            newItem: CategoryUsersResponse.Data.CatUser
        ): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TechViewHolder {
        return TechViewHolder(
            ItemTechBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TechViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}