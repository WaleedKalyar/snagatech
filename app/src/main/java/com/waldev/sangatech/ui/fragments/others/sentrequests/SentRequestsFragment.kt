package com.waldev.sangatech.ui.fragments.others.sentrequests

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.SentRequestsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SentRequestsFragment : Fragment() {
    private var _binding: SentRequestsFragmentBinding? = null
    private val binding get() = _binding!!

    companion object {
        fun newInstance() = SentRequestsFragment()
    }

    private val viewModel: SentRequestsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SentRequestsFragmentBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}