package com.waldev.sangatech.ui.dialogs.drawer

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.DialogNavDrawerBinding
import java.util.*

class NavDrawerDialog(
    context: Context,
    private val onCreateRequestClick: () -> Unit,
    private val onViewSentRequestsClick: () -> Unit,
) : Dialog(context) {
    private var _binding: DialogNavDrawerBinding? = null
    private val binding get() = _binding!!

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        window!!.setWindowAnimations(R.style.rightAnimation)
        _binding = DialogNavDrawerBinding.inflate(LayoutInflater.from(context))
        setContentView(binding.root)
        Objects.requireNonNull(window)?.setBackgroundDrawable(
            ColorDrawable(
                context.resources.getColor(
                    android.R.color.transparent,
                    null,
                )
            )
        )
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        init()
    }

    private fun init() {
        setClickListeners()
    }

    private fun setClickListeners() {
        binding.btnBack.setOnClickListener { dismiss() }
        binding.tvCreateRequest.setOnClickListener {
            onCreateRequestClick()
            dismiss()
        }
        binding.tvSentRequests.setOnClickListener {
            onViewSentRequestsClick()
            dismiss()
        }
    }
}