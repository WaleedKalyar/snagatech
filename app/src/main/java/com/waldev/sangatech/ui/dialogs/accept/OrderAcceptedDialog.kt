package com.waldev.sangatech.ui.dialogs.accept

import android.R
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import com.waldev.sangatech.databinding.DialogOrderAcceptedBinding
import java.util.*

class OrderAcceptedDialog(
    context: Context,
    private val onOrderAccept: () -> Unit
) : Dialog(context) {
    private var _binding: DialogOrderAcceptedBinding? = null
    private val binding get() = _binding!!

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        _binding = DialogOrderAcceptedBinding.inflate(LayoutInflater.from(context))
        setContentView(binding.root)
        Objects.requireNonNull(window)?.setBackgroundDrawable(
            ColorDrawable(
                context.resources.getColor(
                    R.color.transparent,
                    null,
                )
            )
        )
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        init()
    }

    private fun init() {
        setClickListeners()
    }

    private fun setClickListeners() {
        binding.btnDone.setOnClickListener {
            onOrderAccept()
            dismiss()
        }
    }

}