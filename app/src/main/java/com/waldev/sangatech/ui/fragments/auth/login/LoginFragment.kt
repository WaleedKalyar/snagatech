package com.waldev.sangatech.ui.fragments.auth.login

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.LoginFragmentBinding
import com.waldev.sangatech.enums.ResponseStatus
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.activities.main.customer.CustomerMainActivity
import com.waldev.sangatech.ui.activities.main.seller.SellerMainActivity
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.utils.AppConsts
import com.waldev.sangatech.utils.AppConsts.USER_TYPE_WORKER
import com.waldev.sangatech.utils.AppDataStore
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import com.waldev.sangatech.utils.ExtensionUtils.hideWaitingDialogIfVisible
import com.waldev.sangatech.utils.ExtensionUtils.showOrUpdateWaitingDialog
import com.waldev.sangatech.utils.ToastUtility
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment() {
    private var _binding: LoginFragmentBinding? = null
    private val binding get() = _binding!!


    private val viewModel: LoginViewModel by viewModels()

    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }

    @Inject
    lateinit var appDataStore: AppDataStore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LoginFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initCollectors()
        initClickListeners()
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.loginState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    ToastUtility.errorToast(requireContext(), it.message)
                    Timber.d("Error -> ${it.message}")
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Signing In")
                }
                is NetworkResult.Success -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    it.result?.let { result ->
                        if (result.status == ResponseStatus.success) {
                            appDataStore.saveUserModel(result.data.userDetail)
                            ToastUtility.successToast(requireContext(), "You Sign In successfully")
                            navigateToNextScreen(result.data.userDetail)
                        } else {
                            ToastUtility.warningToast(
                                requireContext(),
                                "Your email or password may incorrect"
                            )
                        }
                    }


                }
            }
        }
    }

    private fun navigateToNextScreen(userDetails: LoginResponse.Data.UserDetail) {
        if (userDetails.type == USER_TYPE_WORKER) {
            Timber.d("SingIn -> Worker")
            Intent(requireActivity(), SellerMainActivity::class.java).apply {
                startActivity(this)
                requireActivity().finishAffinity()
            }
        } else if (userDetails.type == AppConsts.USER_TYPE_CUSTOMER) {
            Timber.d("SingIn -> Customer")
            Intent(requireActivity(), CustomerMainActivity::class.java).apply {
                startActivity(this)
                requireActivity().finishAffinity()
            }
        }
    }

    private fun initClickListeners() {
        binding.tvSignUp.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_signUpContainerFragment)
        }

        binding.btnLogin.setOnClickListener {
            validateInputs()
        }
    }

    private fun validateInputs() {
        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        if (email.isEmpty()) {
            binding.inputEmail.error = "Please enter your email"
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            binding.inputEmail.error = "Please enter valid email"
        } else if (password.isEmpty()) {
            binding.inputEmail.error = null
            binding.inputPassword.error = "Please enter your password"
        } else {
            binding.inputEmail.error = null
            binding.inputPassword.error = null
            viewModel.loginUser(email, password)
        }
    }


    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }


}