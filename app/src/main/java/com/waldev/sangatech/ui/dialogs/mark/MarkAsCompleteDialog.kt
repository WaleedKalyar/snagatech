package com.waldev.sangatech.ui.dialogs.mark

import android.R
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import com.waldev.sangatech.databinding.DialogMarkAsCompleteBinding
import com.waldev.sangatech.enums.UserType
import com.waldev.sangatech.responses.work.WorkResponse
import com.waldev.sangatech.utils.DateUtilities
import java.util.*

class MarkAsCompleteDialog(
    context: Context,
    private val order: WorkResponse.Data.WorkReq,
    private val userType: UserType,
    private val onMarkAsComplete: () -> Unit
) : Dialog(context) {
    private var _binding: DialogMarkAsCompleteBinding? = null
    private val binding get() = _binding!!

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        _binding = DialogMarkAsCompleteBinding.inflate(LayoutInflater.from(context))
        setContentView(binding.root)
        Objects.requireNonNull(window)?.setBackgroundDrawable(
            ColorDrawable(
                context.resources.getColor(
                    R.color.transparent,
                    null,
                )
            )
        )
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        init()
    }

    private fun init() {
        initViews()
        setClickListeners()
    }

    @SuppressLint("SetTextI18n")
    private fun initViews() {
        binding.apply {
            tvTitle.text = order.title
            tvAmount.text = String.format("$%.2f", order.amount)
            if (userType == UserType.WORKER) {
                binding.titleOrderBy.text = "Order By"
                tvUserName.text = order.customer_detail.name
            } else {
                binding.titleOrderBy.text = "Order To"
                tvUserName.text = order.worker_detail.name
            }
            tvId.text = "S2OI-${Calendar.getInstance().get(Calendar.YEAR)}-${order.id}"
            tvDate.text = DateUtilities.getDateFromMillis(order.arr_time.toLong())
            tvTime.text = DateUtilities.getTimeFromMillis(order.arr_time.toLong())

            tvDesc.text = order.desc
        }

    }

    private fun setClickListeners() {
        binding.btnCancel.setOnClickListener { dismiss() }
        binding.btnMarkAsComplete.setOnClickListener {
            onMarkAsComplete()
            dismiss()
        }
    }


}