package com.waldev.sangatech.ui.fragments.auth.signup.worker

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.FragmentWorkerSignUpBinding
import com.waldev.sangatech.enums.ResponseStatus
import com.waldev.sangatech.requests.SignUpRequest
import com.waldev.sangatech.responses.categories.CategoriesResponse
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.activities.main.seller.SellerMainActivity
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.ui.fragments.auth.signup.container.SignUpContainerViewModel
import com.waldev.sangatech.utils.AppDataStore
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import com.waldev.sangatech.utils.ExtensionUtils.hideWaitingDialogIfVisible
import com.waldev.sangatech.utils.ExtensionUtils.showOrUpdateWaitingDialog
import com.waldev.sangatech.utils.ToastUtility
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class WorkerSignUpFragment(
    private val onBackPress: () -> Unit
) : Fragment() {
    private var _binding: FragmentWorkerSignUpBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SignUpContainerViewModel by activityViewModels()
    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }

    @Inject
    lateinit var appDataStore: AppDataStore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentWorkerSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initCollectors()
        initClickListeners()
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.categoriesState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    Timber.d("Categories: Error -> ${it.message}")
                    ToastUtility.errorToast(requireContext(), it.message)
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Setting Up")
                }
                is NetworkResult.Success -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    it.result?.let { resp ->
                        loadSpinner(resp.data.catDetail.toMutableList())
                    }
                }
            }
        }
        collectLatestLifecycleFlow(viewModel.singUpState) {
            when (it) {
                is NetworkResult.Error -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    ToastUtility.errorToast(requireContext(), it.message)
                    Timber.d("Error -> ${it.message}")
                }
                is NetworkResult.Idle -> {}
                is NetworkResult.Loading -> {
                    showOrUpdateWaitingDialog(waitingDialog, "Signing Up")
                }
                is NetworkResult.Success -> {
                    hideWaitingDialogIfVisible(waitingDialog)
                    it.result?.let { resp ->
                        Timber.d("SignUP: Worker response -> ${resp.data.userDetail.toString()}")
                        resp.status
                        if (resp.status == ResponseStatus.success) {
                            val userInfo = LoginResponse.Data.UserDetail(
                                address_1 = resp.data.userDetail.address_1,
                                address_2 = resp.data.userDetail.address_2,
                                category = getCategoryIdFromName(resp.data.userDetail.category),
                                created_at = resp.data.userDetail.created_at,
                                desc = "",
                                email = resp.data.userDetail.email,
                                email_verified_at = "",
                                id = resp.data.userDetail.id,
                                latitude = resp.data.userDetail.latitude,
                                longitude = resp.data.userDetail.longitude,
                                name = resp.data.userDetail.name,
                                profile_img = "",
                                profile_url = "",
                                role = resp.data.userDetail.role,
                                status = 0,
                                type = resp.data.userDetail.type,
                                updated_at = resp.data.userDetail.updated_at
                            )
                            Timber.d("SignUp Info -> $userInfo")
                            appDataStore.saveUserModel(userInfo)
                            resp.data.userDetail
                            navigateToMain()
                        } else {
                            ToastUtility.errorToast(
                                requireContext(),
                                "Something went wrong! error-> ${resp.message}"
                            )
                            Timber.d("SingUp: Worker resp -> ${resp.toString()}")
                        }
                    }
                }
            }
        }
    }

    private fun loadSpinner(catDetail: MutableList<CategoriesResponse.Data.CatDetail>) {
        viewModel.categoriesList.addAll(catDetail)
        val adapter: ArrayAdapter<CategoriesResponse.Data.CatDetail> =
            ArrayAdapter<CategoriesResponse.Data.CatDetail>(
                requireContext(),
                R.layout.spinner_item,
                catDetail
            )
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        binding.spCategory.adapter = adapter
    }

    private fun initClickListeners() {
        binding.btnSignUp.setOnClickListener {
            validateInputs()
        }
        binding.tvLogin.setOnClickListener {
            onBackPress()
        }
    }

    private fun navigateToMain() {
        Intent(requireContext(), SellerMainActivity::class.java).apply {
            startActivity(this)
            requireActivity().finishAffinity()
        }
    }

    private fun validateInputs() {
        binding.apply {
            val category = spCategory.selectedItem.toString()
            val email = etEmail.text.toString()
            val name = etName.text.toString()
            val address1 = etAddress1.text.toString()
            val address2 = etAddress2.text.toString()
            val password = etPassword.text.toString()
            val confirmPass = etConfirmPass.text.toString()
            if (category.isEmpty()) {
                ToastUtility.errorToast(requireContext(), "Please select category first")
            } else if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                inputEmail.error = "Email is empty or invalid"
            } else if (name.isEmpty()) {
                inputEmail.error = null
                inputName.error = "Please enter your name"
            } else if (address1.isEmpty()) {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = "Please fill your primary address"
            } else if (address2.isEmpty()) {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = null
                inputAddress2.error = "Please fill your secondary address"
            } else if (password.isEmpty() || password.length < 6) {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = null
                inputAddress2.error = null
                inputPassword.error = "Password must be at least 6 characters"
            } else if (confirmPass != password) {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = null
                inputAddress2.error = null
                inputPassword.error = null
                inputConfirmPass.error = "Password does not match"
            } else {
                inputEmail.error = null
                inputName.error = null
                inputAddress1.error = null
                inputAddress2.error = null
                inputPassword.error = null
                inputConfirmPass.error = null

                viewModel.registerNewUser(
                    SignUpRequest(
                        name = name,
                        email = email,
                        password = password,
                        category = getCategoryIdFromName(category),
                        address1 = address1,
                        address2 = address2,
                        latitude = viewModel.location.latitude,
                        longitude = viewModel.location.longitude
                    )
                )
            }
        }
    }

    private fun getCategoryIdFromName(category: String): Int {
        val cat = viewModel.categoriesList.find { it.name.lowercase() == category.lowercase() }
        return cat?.id ?: 0
    }


    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}