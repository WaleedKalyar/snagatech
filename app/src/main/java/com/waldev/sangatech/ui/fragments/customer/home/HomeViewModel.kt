package com.waldev.sangatech.ui.fragments.customer.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.repos.categories.CategoriesRepo
import com.waldev.sangatech.responses.categories.CategoriesResponse
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.utils.AppDataStore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val categoriesRepo: CategoriesRepo,
    private val appDataStore: AppDataStore
) : ViewModel() {

    private val _catsUiState =
        MutableStateFlow<NetworkResult<CategoriesResponse>>(NetworkResult.Idle())
    val catsUiState = _catsUiState.asStateFlow()

    private val _currentUserState =
        MutableStateFlow(LoginResponse.Data.UserDetail())
    val currentUserState = _currentUserState.asStateFlow()

    init {
        viewModelScope.launch {
            appDataStore.getUserFromDataStore().collectLatest {
                _currentUserState.value = it
            }
        }
        fetchCategories()
    }

    fun fetchCategories() {
        viewModelScope.launch {
            categoriesRepo.getAllCategories().collectLatest {
                _catsUiState.value = it
            }
        }
    }
}