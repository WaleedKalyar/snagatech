package com.waldev.sangatech.ui.dialogs.update

import android.R
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import com.waldev.sangatech.databinding.DialogUpdateInfoBinding
import java.util.*

class UpdateInfoDialog(
    context: Context,
    private val title: String,
    private val lines: Int,
    private val hint: String,
    private val oldInfo: String,
    private val onInfoSubmit: (String) -> Unit
) : Dialog(context) {
    private var _binding: DialogUpdateInfoBinding? = null
    private val binding get() = _binding!!


    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        _binding = DialogUpdateInfoBinding.inflate(LayoutInflater.from(context))
        setContentView(binding.root)
        Objects.requireNonNull(window)?.setBackgroundDrawable(
            ColorDrawable(
                context.resources.getColor(
                    R.color.transparent,
                    null,
                )
            )
        )
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        init()
    }

    private fun init() {
        initViews()
        initClickListeners()
    }

    private fun initClickListeners() {
        binding.btnCancel.setOnClickListener { dismiss() }
        binding.btnDone.setOnClickListener {
            val info = binding.etInfo.text.toString()
            if (info.isEmpty()) {
                binding.inputInfo.error = "Please fill first"
            } else {
                binding.inputInfo.error = null
                onInfoSubmit(info)
                dismiss()
            }
        }
    }

    private fun initViews() {
        binding.apply {
            tvTitle.text = title
            tvTitleInfo.text = title
            etInfo.minLines = lines
            inputInfo.hint = hint
            etInfo.setText(oldInfo)
        }
    }


}