package com.waldev.sangatech.ui.fragments.auth.signup.container

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.models.LocationModel
import com.waldev.sangatech.repos.categories.CategoriesRepo
import com.waldev.sangatech.repos.users.UsersRepo
import com.waldev.sangatech.requests.SignUpRequest
import com.waldev.sangatech.responses.categories.CategoriesResponse
import com.waldev.sangatech.responses.user.SignUpResponse
import com.waldev.sangatech.sealed.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpContainerViewModel @Inject constructor(
    private val usersRepo: UsersRepo,
    private val categoriesRepo: CategoriesRepo
) : ViewModel() {
    var location: LocationModel = LocationModel()
    var categoriesList: MutableList<CategoriesResponse.Data.CatDetail> = ArrayList()
    private val _signUpState = MutableStateFlow<NetworkResult<SignUpResponse>>(NetworkResult.Idle())
    val singUpState = _signUpState.asStateFlow()

    private val _categoriesState =
        MutableStateFlow<NetworkResult<CategoriesResponse>>(NetworkResult.Idle())
    val categoriesState = _categoriesState.asStateFlow()


    init {
        fetchAllCategories()
    }

    private fun fetchAllCategories() {
        viewModelScope.launch {
            categoriesRepo.getAllCategories().collectLatest {
                _categoriesState.value = it
            }
        }
    }

    fun registerNewUser(request: SignUpRequest) {
        viewModelScope.launch {
            usersRepo.registerUserWithEmailAndPassword(request)
                .collectLatest {
                    _signUpState.value = it
                }
        }
    }


}