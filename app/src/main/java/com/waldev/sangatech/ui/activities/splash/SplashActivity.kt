package com.waldev.sangatech.ui.activities.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.waldev.sangatech.databinding.ActivitySplashBinding
import com.waldev.sangatech.ui.activities.auth.AuthActivity
import com.waldev.sangatech.ui.activities.main.customer.CustomerMainActivity
import com.waldev.sangatech.ui.activities.main.seller.SellerMainActivity
import com.waldev.sangatech.utils.AppConsts.USER_TYPE_CUSTOMER
import com.waldev.sangatech.utils.AppConsts.USER_TYPE_WORKER
import com.waldev.sangatech.utils.AppDataStore
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {
    private var _binding: ActivitySplashBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SplashViewModel by viewModels()

    @Inject
    lateinit var appDataStore: AppDataStore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        initCollectors()
    }

    private fun initCollectors() {
        lifecycleScope.launch {
            viewModel.splashUiState.collectLatest {
                if (it) {
                    navigateToNextScreen()
                }
            }
        }
    }

    private fun navigateToNextScreen() {
        lifecycleScope.launch {
            appDataStore.getUserFromDataStore().collectLatest { userDetails ->
                if (userDetails.id == 0 && userDetails.email.isEmpty()) {
                    Intent(this@SplashActivity, AuthActivity::class.java).apply {
                        startActivity(this)
                        finishAffinity()
                    }
                } else {
                    if (userDetails.type == USER_TYPE_WORKER) {
                        Intent(this@SplashActivity, SellerMainActivity::class.java).apply {
                            startActivity(this)
                            finishAffinity()
                        }
                    } else if (userDetails.type == USER_TYPE_CUSTOMER) {
                        Intent(this@SplashActivity, CustomerMainActivity::class.java).apply {
                            startActivity(this)
                            finishAffinity()
                        }
                    }
                }
            }
        }
    }
}