package com.waldev.sangatech.ui.fragments.others.feedback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.FeedBackFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FeedBackFragment : Fragment() {
    private var _binding: FeedBackFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: FeedBackViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FeedBackFragmentBinding.inflate(inflater,container,false)
        return binding.root
    }


    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}