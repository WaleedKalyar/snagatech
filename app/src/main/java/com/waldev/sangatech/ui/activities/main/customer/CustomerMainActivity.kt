package com.waldev.sangatech.ui.activities.main.customer

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.ActivityCustomerMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CustomerMainActivity : AppCompatActivity() {
    private var _binding: ActivityCustomerMainBinding? = null
    private val binding get() = _binding!!

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityCustomerMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        initNavigation()
        initListeners()
    }

    private fun initNavigation() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.customer_main_fragment_container) as NavHostFragment
        navController = navHostFragment.navController
        NavigationUI.setupWithNavController(binding.navView, navController)
    }


    private fun initListeners() {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.homeFragment -> binding.navView.visibility = View.VISIBLE
                R.id.ordersFragment2 -> binding.navView.visibility = View.VISIBLE
                R.id.inboxFragment2 -> binding.navView.visibility = View.VISIBLE
                R.id.profileFragment2 -> binding.navView.visibility = View.VISIBLE
                else -> binding.navView.visibility = View.GONE
            }
        }
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}