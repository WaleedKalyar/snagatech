package com.waldev.sangatech.ui.fragments.customer.tech

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.waldev.sangatech.R
import com.waldev.sangatech.databinding.TechsFragmentBinding
import com.waldev.sangatech.enums.ResponseStatus
import com.waldev.sangatech.sealed.NetworkResult
import com.waldev.sangatech.ui.dialogs.wait.WaitingDialog
import com.waldev.sangatech.ui.fragments.customer.tech.adapters.TechsAdapter
import com.waldev.sangatech.utils.AppConsts
import com.waldev.sangatech.utils.AppConsts.CATEGORY_ID
import com.waldev.sangatech.utils.AppConsts.CATEGORY_NAME
import com.waldev.sangatech.utils.AppConsts.WORKER_ID
import com.waldev.sangatech.utils.AppConsts.WORKER_NAME
import com.waldev.sangatech.utils.ExtensionUtils.collectLatestLifecycleFlow
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TechsFragment : Fragment() {
    private var _binding: TechsFragmentBinding? = null
    private val binding get() = _binding!!


    private val viewModel: TechsViewModel by viewModels()
    private val waitingDialog: WaitingDialog by lazy { WaitingDialog(requireContext()) }

    private lateinit var adapter: TechsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = TechsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initArguments()
        initAdapter()
        initCollectors()
        initClickListeners()
    }

    private fun initClickListeners() {
        binding.btnBack.setOnClickListener { findNavController().navigateUp() }
        binding.btnRetry.setOnClickListener { viewModel.fetchUsersByCategory() }
    }

    private fun initAdapter() {
        adapter = TechsAdapter(viewModel.catName,
            onChatClick = {

            },
            onSendOfferClick = {
                val bundle = Bundle()
                bundle.putInt(WORKER_ID, it.id)
                bundle.putString(WORKER_NAME, it.name)
                bundle.putInt(CATEGORY_ID, viewModel.catId)
                findNavController().navigate(
                    R.id.action_techsFragment_to_createRequestFragment,
                    bundle
                )
            },
            onUserClick = {
                val bundle = Bundle()
                bundle.putInt(AppConsts.USER_ID, it)
                findNavController().navigate(
                    R.id.action_techsFragment_to_otherProfileFragment,
                    bundle
                )
            })
        binding.recyclerTechs.adapter = adapter
    }

    private fun initCollectors() {
        collectLatestLifecycleFlow(viewModel.catUsersState) {
            when (it) {
                is NetworkResult.Error -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.VISIBLE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerTechs.visibility = View.GONE
                    }
                }
                is NetworkResult.Idle -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerTechs.visibility = View.GONE
                    }
                }
                is NetworkResult.Loading -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.startShimmer()
                        shimmerLayout.visibility = View.VISIBLE
                        recyclerTechs.visibility = View.GONE
                    }
                }
                is NetworkResult.Success -> {
                    binding.apply {
                        rlNoData.visibility = View.GONE
                        rlRetry.visibility = View.GONE
                        shimmerLayout.stopShimmer()
                        shimmerLayout.visibility = View.GONE
                        recyclerTechs.visibility = View.GONE
                    }
                    it.result?.let { result ->
                        if (result.status == ResponseStatus.success && result.data.catUsers.isNotEmpty()) {
                            binding.recyclerTechs.visibility = View.VISIBLE
                            adapter.submitList(result.data.catUsers)
                        } else {
                            binding.rlNoData.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }
    }

    private fun initArguments() {
        viewModel.catId = requireArguments().getInt(CATEGORY_ID, 0)
        viewModel.catName = requireArguments().getString(CATEGORY_NAME, "")
        binding.tvCatName.text = viewModel.catName
        viewModel.fetchUsersByCategory()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}