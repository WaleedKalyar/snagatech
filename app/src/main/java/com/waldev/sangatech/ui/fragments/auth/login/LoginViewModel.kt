package com.waldev.sangatech.ui.fragments.auth.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.repos.users.UsersRepo
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.sealed.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val usersRepo: UsersRepo
) : ViewModel() {

    private val _loginState = MutableStateFlow<NetworkResult<LoginResponse>>(NetworkResult.Idle())

    val loginState = _loginState.asStateFlow()


    fun loginUser(email: String, password: String) {
        viewModelScope.launch {
            usersRepo.loginUserWithEmailAndPassword(email, password)
                .collectLatest {
                    _loginState.value = it
                }
        }
    }

}