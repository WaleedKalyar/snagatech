package com.waldev.sangatech.ui.fragments.auth.signup.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.waldev.sangatech.ui.fragments.auth.signup.customer.CustomerSignUpFragment
import com.waldev.sangatech.ui.fragments.auth.signup.worker.WorkerSignUpFragment

class SignUpPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle, private val onBackPress: () -> Unit) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    private val fragmentList: MutableList<Fragment> = ArrayList()

    init {
        fragmentList.add(CustomerSignUpFragment(onBackPress))
        fragmentList.add(WorkerSignUpFragment(onBackPress))
    }

    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }



}