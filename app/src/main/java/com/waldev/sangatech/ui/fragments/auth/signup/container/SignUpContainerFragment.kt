package com.waldev.sangatech.ui.fragments.auth.signup.container

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.waldev.sangatech.databinding.SignUpContainerFragmentBinding
import com.waldev.sangatech.models.LocationModel
import com.waldev.sangatech.ui.fragments.auth.signup.adapters.SignUpPagerAdapter
import com.waldev.sangatech.utils.SingleTimeLocationProvider
import com.waldev.sangatech.utils.ToastUtility
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class SignUpContainerFragment : Fragment() {
    private var _binding: SignUpContainerFragmentBinding? = null
    private val binding get() = _binding!!


    private val viewModel: SignUpContainerViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SignUpContainerFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkPermissionAndRequestLocation()
        initPager()
    }


    private fun initPager() {
        val pagerAdapter = SignUpPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle) {
            findNavController().navigateUp()
        }
        binding.pager.adapter = pagerAdapter
        TabLayoutMediator(binding.tablayout, binding.pager) { tab, position ->
            if (position == 0) {
                tab.text = "Customer"
            } else {
                tab.text = "Worker"
            }
        }.attach()
        setTabSpacing()
    }

    private fun setTabSpacing() {
        val betweenSpace = 30
        val slidingTabStrip = binding.tablayout.getChildAt(0) as ViewGroup
        for (i in 0 until slidingTabStrip.childCount - 1) {
            val v = slidingTabStrip.getChildAt(i)
            val params = v.layoutParams as MarginLayoutParams
            params.rightMargin = betweenSpace
        }
    }


    private fun checkPermissionAndRequestLocation() {
        Dexter.withContext(requireContext())
            .withPermissions(
                mutableListOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    if (p0 == null) {
                        ToastUtility.errorToast(requireContext(), "Permissions not granted!")
                    }
                    p0?.let {
                        if (it.areAllPermissionsGranted()) {
                            sendLocation()
                        } else {
                            requireActivity().onBackPressed()
                            ToastUtility.errorToast(
                                requireContext(),
                                "Some permissions not granted"
                            )
                        }
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {

                }

            })
            .withErrorListener {
                ToastUtility.errorToast(requireContext(), it.name)
            }
            .check()
    }

    private fun sendLocation() {
        SingleTimeLocationProvider.requestSingleUpdate(requireContext(),
            object : SingleTimeLocationProvider.LocationCallback {
                override fun onLocationAvailable(location: LocationModel?) {
                    // super.onLocationAvailable(location)
                    Timber.d("Location: is lat -> ${location?.latitude}, long -> ${location?.longitude}")
                    val json = Gson().toJson(location)
                    if (location != null) {
                        viewModel.location = location
                    }
                }

                override fun onLocationPermissionError() {
                    Timber.d("Location: Permission error")
                    ToastUtility.errorToast(requireContext(), "Location permission denied")
                    // super.onLocationPermissionError()
                }

                override fun onLocationProvidersNotEnable() {
                    Timber.d("Location: Provider not enable")
                    ToastUtility.errorToast(
                        requireContext(),
                        "Location is not enabled on your device"
                    )
                    // super.onLocationProvidersNotEnable()
                }

                override fun onLocationNotAvailable() {
                    Timber.d("Location: Not available")
                    ToastUtility.errorToast(
                        requireContext(),
                        "Location is not available, please enable location on your device first"
                    )
                    //super.onLocationNotAvailable()
                }

                override fun onLocationRequestComplete() {
                    Timber.d("Location: request completed")
                    //super.onLocationRequestComplete()
                }
            })
    }


    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }


}