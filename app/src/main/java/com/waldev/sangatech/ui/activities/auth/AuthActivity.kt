package com.waldev.sangatech.ui.activities.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.waldev.sangatech.databinding.ActivityAuthBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthActivity : AppCompatActivity() {
    private var _binding: ActivityAuthBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun init() {

    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}