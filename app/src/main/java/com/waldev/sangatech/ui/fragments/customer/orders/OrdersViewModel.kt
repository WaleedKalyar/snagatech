package com.waldev.sangatech.ui.fragments.customer.orders

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waldev.sangatech.responses.user.LoginResponse
import com.waldev.sangatech.utils.AppDataStore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OrdersViewModel @Inject constructor(
    private val appDataStore: AppDataStore
) : ViewModel() {

    private val _userDetails = MutableStateFlow(LoginResponse.Data.UserDetail())
    val userDetails = _userDetails.asStateFlow()

    init {
        viewModelScope.launch {
            appDataStore.getUserFromDataStore().collectLatest {
                _userDetails.value = it
            }
        }
    }
}